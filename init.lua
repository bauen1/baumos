-- init.lua - kernel entry point
-- vim: set tabstop=2 shiftwidth=2 expandtab :

---
-- @module kernel

local computer = computer
local component = component

-- baumos is only compatible with lua 5.3 (compat for the bit32 library is present for user programs)
if computer.setArchitecture then
  computer.setArchitecture("Lua 5.3")
end

local __bootfs_addr = computer.getBootAddress()
local function bootfs_invoke(...)
  return component.invoke(__bootfs_addr, ...)
end

local function __readfile(file)
  checkArg(1, file, "string")
  local f, err = bootfs_invoke("open", file)
  if not f then
    return nil, err
  end
  local buf = {}
  while true do
    local v
    v, err = bootfs_invoke("read", f, math.huge)
    if not v then
      bootfs_invoke("close", f)
      if err then
        return nil, err
      end
      break
    end
    buf[#buf + 1] = v
  end
  return table.concat(buf)
end

-- _K is a read-only environment for kernel modules (and errors on access to undefined variables)
local _K = {}
_K._G = _K
_K = setmetatable(_K, {
  __index = function(_, n)
    local v = rawget(_G, n)
    if v == nil then
      error("variable '" .. tostring(n) .. "' is not declared", 2)
    end
    return v
  end,
  __newindex = function(_, n, v)
    error("assign to global variable '" .. tostring(n) .. "' with value '" .. tostring(v) .. "'", 2)
  end
})

_G.klog = function() end -- no-op

local function freeMemory()
  if collectgarbage then
    for _ = 0, 10 do
      collectgarbage()
    end
  end
  return computer.freeMemory()
end

local function load_module(name, ...)
  checkArg(1, name, "string")

  local path = "/sys/" .. name .. ".lua"

  klog("free memory: %fkb", freeMemory() / 1024)

  klog("read '%s'", path)

  local f, err = __readfile(path)
  if not f then
    return nil, string.format("error loading module: %s: error reading file %s: %s", name, path, tostring(err))
  end

  klog("load module '%s'", name)

  local mod
  mod, err = load(f, "=" .. tostring(path), "bt", _K)

  if not mod then
    return nil, string.format("error loading module: %s: %s", name, tostring(err))
  end

  local v = table.pack(pcall(mod, ...))
  if v[1] then
    klog("free memory: %fkb", freeMemory() / 1024)
    return table.unpack(v, 2, v.n)
  else
    return nil, string.format("error executing module %s: %s", name, tostring(v[2]))
  end
end

local dmesg = assert(load_module("dmesg"))
_G.dmesg = dmesg
_G.klog = dmesg.log

klog("booting from %s", __bootfs_addr)

local kevent = assert(load_module("kevent"))
_G.kevent = kevent

local keymap = assert(load_module("keymap"))
_G.keymap = keymap

-- TTY driver, one instance per screen / gpu pair
local tty = assert(load_module("tty"))
local boot_tty
do
  boot_tty = assert(tty.new(
    assert(component.proxy(assert(component.list("gpu", true)()))),
    assert(component.proxy(assert(component.list("screen", true)())))
  ))
end

local sandbox = assert(load_module("sandbox"))
_G.sandbox = sandbox

local vfs = assert(load_module("vfs"))
_G.vfs = vfs

local devfs = assert(load_module("devfs"))
_G.devfs = devfs
devfs.data.tty = boot_tty.devfs_file

do
  -- Mount /
  klog("mounting bootfs %s on /", __bootfs_addr)
  assert(vfs.mount(assert(component.proxy(__bootfs_addr)), "/"))

  -- Mount /tmp if possible
  local tmpfs_addr = computer.tmpAddress()
  if tmpfs_addr then
    klog("mounting tmpfs %s on /tmp", tmpfs_addr)
    local tmpfs_proxy, err = component.proxy(tmpfs_addr)
    if not tmpfs_proxy then
      klog("error mounting /tmp: %s", tostring(err))
    else
      local success
      success, err = vfs.mount(component.proxy(tmpfs_addr), "/tmp")
      if not success then
        klog("error mouning /tmp: %s", tostring(err))
      end
    end
  end

  -- Mount /dev: without this /bin/init.lua will fail
  assert(vfs.mount(devfs.proxy, "/dev"))
end

do
  -- redirect dmesg to console
  local dmesg_handle = assert(devfs.proxy.open("tty"))

  dmesg.__status = function(...)
    return devfs.proxy.write(dmesg_handle, ...)
  end
end

local buffer = assert(load_module("buffer"))
_G.buffer = buffer

local process = assert(load_module("process"))
_G.process = process

local user = assert(load_module("user"))
_G.user = user

local net = assert(load_module("net"))
_G.net = net

local _U = user.new()

local init_proc, err = process.new(
  assert(_U.loadfile("/bin/init.lua"), "loadfile /lib/init.lua failed"),
  "init",
  false,
  nil,
  "/bin/init.lua",
  "/bin/init.lua"
)

if not init_proc then
  error("failed to spawn /bin/init.lua: " .. tostring(err))
end

for c, t in component.list() do
  computer.pushSignal("component_added", c, t)
end

local lastYield = 0
local function handleSignals()
  local deadline = math.huge
  for _, proc in pairs(process.list) do
    if proc.thread then
      deadline = math.min(deadline, proc.deadline)
    end
  end

  assert(deadline >= 0)

  local signal = table.pack(computer.pullSignal(deadline))
  lastYield = computer.uptime()

  if not signal[1] or signal[1] == "timeout" then
    return
  end

  kevent.handle_signal(signal)
end

local function checkTimeout()
  -- check if we need to yield to avoid getting killed (5s)
  local yieldTime = 3
  return (computer.uptime() - lastYield > yieldTime)
end

klog("enter scheduler")
while true do
  handleSignals()

  for proc in process.iter_resumable_procs() do
    dmesg.debug("schedule %s", proc)
    proc.reason = nil
    proc.timeout = nil
    proc.filter = nil

    local t1 = os.clock()
    proc.clock_last = t1
    process.cproc = proc
    local state, reason, timeout, filter = coroutine.resume(proc.thread, table.unpack(proc.args))
    proc.clock_last = nil
    process.cproc = nil
    local t2 = os.clock()

    proc.args = nil

    if (not state) or (not reason) or (coroutine.status(proc.thread) == "dead") or (reason == "exit") then
      local success = timeout
      assert(type(success) == "boolean")
      if not success then
        local err, trace = filter[1], filter[2]
        klog("proc %s terminate: error '%s' trace '%s'", proc, tostring(err), tostring(trace))
        assert(proc:kill(table.pack(nil, err, trace)))
      else
        klog("proc %s terminated:", proc)
        for k,v in pairs(filter) do
          klog("%s => %s", tostring(k), tostring(v))
        end
        assert(proc:kill(filter))
      end
    else
      assert(type(reason) == "string")
      local valid_reasons = {
        sleep = true,
        wait = true,
      }
      if not valid_reasons[reason] then
        error("invalid reason" .. tostring(reason))
      end
      assert(type(timeout) == "number")
      proc.deadline = computer.uptime() + timeout
      proc.reason = reason
      proc.filter = filter
    end

    if checkTimeout() then
      break
    end
  end
end
