-- .luacheckrc - luacheck configuration
-- vim: set tabstop=2 shiftwidth=2 expandtab syntax=lua :
--

local func = { read_only = true, other_fields = false }

stds.opencomputers = {
  read_globals = {
    checkArg = func,
    computer = {
      fields = {
        address = func,
        tmpAddress = func,
        freeMemory = func,
        totalMemory = func,
        energy = func,
        maxEnergy = func,
        uptime = func,
        shutdown = func,
        --getBootAddress = func, -- provided by bios.lua: deprecated
        --setBootAddress = func, -- provided by bios.lua: deprecated
        users = func,
        addUser = func,
        removeUser = func,
        pushSignal = func,
        pullSignal = func,
        beep = func,
        setArchitecture = func,
        getArchitecture = func,
      },
    },
    component = {
      fields = {
        doc = func,
        fields = func,
        invoke = func,
        list = func,
        methods = func,
        proxy = func,
        slot = func,
        type = func,
      },
    },
    unicode = {
      fields = {
        char = func,
        charWidth = func,
        isWide = func,
        len = func,
        lower = func,
        reverse = func,
        sub = func,
        upper = func,
        wlen = func,
        wtrunc = func,
      },
    },
  },
}

-- kernel: /sys init.lua
stds.kernel = {
  read_globals = {
    dmesg = {
      fields = {
        log = func,
        debug = func,
      },
    },
    klog = func,
    kevent = {
      fields = {
        listen = func,
        signal = func,
      },
    },
    "keymap",
    tty = {
      -- TODO
    },
    sandbox = {
      fields = {
        wrap_type = func,
        unwrap_type = func,
        handle = {
          fields = {
            new = func,
            cleanup = func,
            unwrap = func,
            is_open = func,
            clone = func,
          },
        },
        getmetatable = func,
        type = func,
        checkArg = func,
      },
    },
    vfs = {
      fields = {
        segments_append_path = func,
        segments_resolve = func,
        path_to_segments = func,
        canonical = func,
        concat = func,
        remove = func,
        open = func,
        realPath = func,
        path = func,
        name = func,
        exists = func,
        isDirectory = func,
        list = func,
        makeDirectory = func,
        lastModified = func,
        mounts = func,
        isLink = func,
        link = func,
        size = func,
      },
      -- TODO
    },
    devfs = {
      -- TODO
    },
    buffer = {
      fields = {
        new = func,
        pipe = func,
      },
    },
    process = {
      fields = {
        cproc = {
          fields = {
            "pid",
            io_input = { read_only = false },
            io_output = { read_only = false },
            io_error = { read_only = false },
            getenv = func,
            setenv = func,
            "clock_last",
            "clock",
          },
        },
        iter_resumable_procs = func,
        exit = func,
        sleep = func,
        wait = func,
        notify = func,
        new = func,
      },
    },
    user = {
      fields = {

      },
    },
    net = {
      fields = {
        list_modems = func,
        socket = {
          fields = {
            new = func,
          }
        },
      },
    },
  },
}

-- user: /lib /bin
stds.user = {
  read_globals = {
    os = {
      fields = {
        getenv = func,
        setenv = func,
        sleep = func,
        spawnp = func,
        wait = func,
      },
    },
  },
}

std = "lua53c+opencomputers"
files["init.lua"].std = std.."+kernel"
files["sys/**/*.lua"].std = std.."+kernel"
files["lib/**/*.lua"].std = std.."+user"
files["bin/**/*.lua"].std = std.."+user"
