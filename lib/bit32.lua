-- lib/bit32.lua
-- vim: ts=2 sw=2
-- lua5.3 <-> lua5.2 compat
if bit32 then
    return bit32
end

local bit32 = {}

function bit32.band(a, b)
  return a & b
end

function bit32.bor(a, b)
  return a | b
end

function bit32.bxor(a, b)
  return a ~ b
end

function bit32.bnot(a)
  return ~a
end

function bit32.rshift(a, n)
  return a >> n
end

function bit32.lshift(a, n)
  return a << n
end

return bit32
