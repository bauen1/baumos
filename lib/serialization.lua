-- lib/serialization.lua - serialize lua objects
-- vim: set tabstop=2 shiftwidth=2 expandtab :
--
local serialization = {}

-- See OpenOS for details

local function local_pairs(tbl)
  local mt = getmetatable(tbl)
  return (mt and mt.__pairs or pairs)(tbl)
end

-- This method does so many things at once, it's not even funny
function serialization.serialize(value, pretty)
  local kw = {
    ["and"]=true, ["break"]=true, ["do"]=true, ["else"]=true,
    ["elseif"]=true, ["end"]=true, ["false"]=true, ["for"]=true,
    ["function"]=true, ["goto"]=true, ["if"]=true, ["in"]=true,
    ["local"]=true, ["nil"]=true, ["not"]=true, ["or"]=true,
    ["repeat"]=true, ["return"]=true, ["then"]=true, ["true"]=true,
    ["until"]=true, ["while"]=true,
  }
  local id = "^[%a_][%w_]*$"
  local ts = {}
  local result_pack = {}
  local function recurse(v, depth)
    local t = type(v)
    if t == "number" then
      if v ~= v then
        table.insert(result_pack, "0/0")
      elseif v == math.huge then
        table.insert(result_pack, "math.huge")
      elseif v == -math.huge then
        table.insert(result_pack, "-math.huge")
      else
        table.insert(result_pack, tostring(v))
      end
    elseif t == "string" then
      table.insert(result_pack, (string.format("%q", v):gsub("\\\n", "\\n")))
    elseif t == "nil" or t == "boolean" or
      pretty and (t ~= "table" or (getmetatable(v) or {}).__tostring) then
      table.insert(result_pack, tostring(v))
    elseif t == "table" then
      if ts[v] then
        if pretty then
          table.insert(result_pack, "recursion")
          return
        else
          error("table with cycles are not supported")
        end
      end
      ts[v] = true
      local f
      if pretty then
        local ks, sks, oks = {}, {}, {}
        for k in pairs(v) do
          if type(k) == "number" then
            table.insert(ks, k)
          elseif type(k) == "string" then
            table.insert(sks, k)
          else
            table.insert(oks, k)
          end
        end
        table.sort(ks)
        table.sort(sks)
        for _, k in ipairs(sks) do
          table.insert(ks, k)
        end
        for _, k in ipairs(oks) do
          table.insert(ks, k)
        end
        local n = 0
        f = table.pack(function()
          n = n + 1
          local k = ks[n]
          return k, v[k]
        end)
      else
        f = table.pack(local_pairs(v))
      end
      local i = 1
      table.insert(result_pack, "{")
      local first = true
      for k, tbl in table.unpack(f) do
        if not first and pretty then
          table.insert(result_pack, "\n" .. string.rep(" ", depth))
        end
        first = nil
        local tk = type(k)
        if tk == "number" and k == i then
          i = i + 1
          recurse(tbl, depth + 1)
        else
          if tk == "string" and not kw[k] and string.match(k, id) then
            table.insert(result_pack, k)
          else
            table.insert(result_pack, "[")
            recurse(k, depth + 1)
            table.insert(result_pack, "]")
          end
          table.insert(result_pack, "=")
          recurse(tbl, depth + 1)
        end
      end
      ts[v] = nil -- allow writing same table more than once
      table.insert(result_pack, "}")
    else
      error("unsupported type: " .. t)
    end
  end
  recurse(value, 1)
  local result = table.concat(result_pack)
  if pretty then
    local limit = type(pretty) == "number" and pretty or 10
    local truncate = 0
    while limit > 0 and truncate do
      truncate = string.find(result, "\n", truncate + 1, true)
      limit = limit - 1
    end
    if truncate then
      return result:sub(1, truncate) .. "..."
    end
  end
  return result
end

function serialization.unserialize(data)
  checkArg(1, data, "string")
  -- ugh
  local result, err = load("return " .. data, "=data", "t", { math = { huge = math.huge } })
  if not result then
    return nil, err
  end
  local ok, v = pcall(result)
  if not ok then
    return nil, v
  end
  return v
end

return serialization
