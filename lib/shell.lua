-- lib/shell.lua - shell library
-- vim: set tabstop=2 shiftwidth=2 expandtab :
--

local shell = {}

function shell.parse(name, ...)
  local params = table.pack(...)
  local args, options = {}, {}
  local doneWithOpts = false

  for i = 1, params.n do
    local param = params[i]
    if not doneWithOpts and type(param) == "string" then
      if param == "--" then
        doneWithOpts = true
      elseif param:sub(1, 2) == "--" then
        local key, value = param:match("%-%-(.-)=(.*)")
        if not key then
          key, value = param:sub(3), true
        end
        options[key] = value
      elseif param:sub(1, 1) == "-" and param ~= "-" then
        for j = 2, unicode.len(param) do
          options[unicode.sub(param, j, j)] = true
        end
      else
        table.insert(args, param)
      end
    else
      table.insert(args, param)
    end
  end

  args.n = #args

  return name, args, options
end

return shell
