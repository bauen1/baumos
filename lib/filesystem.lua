-- /lib/filesystem.lua
-- vim: ts=2 sw=2
--

-- FIXME: this might not be the best idea
local filesystem = require("libcore").filesystem

filesystem.open = io.open

function filesystem.copy(fromPath, toPath)
  local data = false
  local output
  local input, err = io.open(fromPath, "rb")
  if input then
    output, err = io.open(toPath, "wb")
    if output then
      repeat
        data, err = input:read(4096)
        if not data then break end
        data, err = output:write(data)
        if not data then data, err = false, "failed to write" end
      until not data
      output:close()
    end
    input:close()
  end
  return data == nil, err
end

return filesystem
