-- lib/libedit.lua - read line utilities
-- vim: set tabstop=2 shiftwidth=2 expandtab :
--

local libedit = {}

function libedit.draw_line(buffer, pos)
  checkArg(1, buffer, "string")
  checkArg(2, pos, "number")
  assert(pos > 0)

  -- restore cursor
  io.write("\0278")

  io.write(buffer)

  -- erase everything after our buffer
  -- XXX: The space is to ensure that the last character is actually erased,
  -- even if it is on the next line
  io.write(" \027[K")

  -- restore cursor
  io.write("\0278")

  -- now we need to move the cursor to pos
  -- we can do this without having to know the width, heigth and real position of the cursor
  -- by simply redrawing the buffer up to pos
  -- FIXME: this might not be the best thing in terms of performance
  io.write(unicode.sub(buffer, 1, pos - 1))
end

function libedit.erase_line(buffer)
  checkArg(1, buffer, "string")

  -- restore cursor
  io.write("\0278")

  io.write(string.rep(" ", unicode.len(buffer)))
end

function libedit.readline(history, tab_complete)
  checkArg(1, history, "table", "nil")
  checkArg(2, tab_complete, "function", "nil")

  local buf = "" -- line buffer
  local pos = 1 -- position of the cursor inside the line buffer

  local history_index = #history + 1
  local final_history_index = history_index

  local hints
  local hints_index = 1

  -- during hinting these include the current hint at the correct position
  local hint_buf
  local hint_pos

  -- Save the cursor position
  io.write("\0277")

  while true do
    local hint = hints and hints[hints_index]
    if hint_buf then
      libedit.draw_line(hint_buf, hint_pos)
    else
      libedit.draw_line(buf, pos)
    end

    local c, err = io.read(1)
    if not c then -- end of file or error
      return nil, err
    end

    if c == "\t" then -- tab complete
      if tab_complete then
        if not hints then
          hints = tab_complete(buf, pos)
        else
          hints_index = (1 + hints_index) % (#hints + 1)
          --hints_index = (hints_index + 1) % (#hints + 1)
        end

        if hints then
          local hint = hints[hints_index]

          if not hint then
            hint = { buf, pos }
          end

          hint_buf = hint[1]
          hint_pos = hint[2]
        end
      end
    else
      if hint_buf then
        buf = hint_buf
        pos = hint_pos
      end
      hints = nil
      hint_buf = nil
      hint_pos = nil

      if c == "\n" then -- newline, end of input
        io.write("\n")
        break
      elseif c == "\27" then -- special key
        local c1
        c1, err = io.read(1)
        if not c1 then return nil, err end

        if c1 == "[" then
          local c2
          c2, err = io.read(1)
          if not c2 then return nil, err end

          if c2 == "C" then -- cursor right
            if unicode.len(buf) >= pos then
              pos = pos + 1
            end
          elseif c2 == "D" then -- cursor left
            if pos > 1 then
              pos = pos - 1
            end
          elseif c2 == "A" then -- cursor up
            if history and history_index > 1 then
              history[history_index] = buf
              history_index = history_index - 1
              buf = history[history_index]
              pos = unicode.len(buf) + 1
            end
          elseif c2 == "B" then -- cursor down
            if history and history_index < #history then
              history[history_index] = buf
              history_index = history_index + 1
              buf = history[history_index] or ""
              pos = unicode.len(buf) + 1
            end
          end

          if c2 == "1" or c2 == "4" then
            local c3 = io.read(1)
            if c3 == "~" then
              if c2 == "1" then -- home
                pos = 1
              elseif c2 == "4" then -- end
                pos = unicode.len(buf) + 1
              end
            else
              buf = unicode.sub(buf, 1, pos - 1) .. c .. c2 .. c3 .. unicode.sub(buf, pos)
              pos = pos + 3
            end
          end
        else
          buf = unicode.sub(buf, 1, pos - 1) .. c .. c1 .. unicode.sub(buf, pos)
          pos = pos + 2
        end
      elseif c == "\b" then -- backspace
        if pos > 1 then
          buf = unicode.sub(buf, 1, pos - 2) .. unicode.sub(buf, pos)
          pos = pos - 1
        end
      elseif c == "\127" then -- delete
        if unicode.len(buf) >= pos then
          buf = unicode.sub(buf, 1, pos - 1) .. unicode.sub(buf, pos + 1)
        end
      else
        buf = unicode.sub(buf, 1, pos - 1) .. c .. unicode.sub(buf, pos)
        pos = pos + 1
      end
    end
  end

  if history and buf ~= "" and unicode.len(buf) > 0 then
    history[final_history_index] = buf
  end

  return buf
end

return libedit
