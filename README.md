# OpenComputers OS (test)

## TODO List

- [ ] Ctrl+C, Ctrl+D support
- [ ] overhaul the concepts (of wait, etc..) and make the APIs more clearly defined
- [ ] callback support

### Thoughs:

wrap `coroutine.yield` calls in the kernel into a `process.yield` call that will check for queued event handlers on return of `"event"`

```Lua
-- Example:

function process.yield(...)
  local result
  while true do
    result = table.pack(coroutine.yield(...))
    if result[1] == "event" then
      process.runQueuedEvents()
    else
      break
    end
  end
  return table.unpack(result)
end

```

## Goals

* Follow the KISS principles
* Be faster than OpenOS
* Be more secure

## Code standards

### Errors

Prefer returning `nil, "error message"` on error instead of `error("error message")`:
Because error handleing using pcall is just bad:

#### Don't do this

```lua
function ifail()
  if true then
    error("failure")
  end

  return "value"
end

local sucess, value_or_error = pcall(ifail())
if not success then
  print("Error: ", value_or_error)
end
```

#### Do this

```lua
function ifail()
  if true then
    return nil, "failure"
  end

  return "value"
end

local value, err = ifail()
if not value then
  print("error: ", err)
end
```

## Filesystem Layout

```
/bin: binaries
/etc: system configuration
/lib: libraries
/sys: kernel
````

## Design

### Security

#### Sandbox

The entire user code is run in a sandbox that has no direct access to components.

#### Namespaces

##### modules

share `require "module"`s

##### filesystem

Like chroot

### Input / Output

#### Files

Always exposed as a handle of a buffer

#### Sockets

TODO: Allow listening on the filesystem

### Handles

Every Ressource is a handle (eg. socket, file)
Every handle can be opened to "clone" it (get another reference) and closed to return this reference, eg:

```lua
local f = assert(io.open("/tmp/a", "w")) -- open a (file) handle

local f2 = assert(f:open()) -- clone this handle

assert(f:close()) -- close the original

assert(f2:write("test")) -- use the clone

assert(f2:close()) -- return the last open handle (our clone)
```

Handles can be passed between processes (by some means), e.g:

```lua
local f = assert(io.open("/tmp/a", "w")) -- open a (file) handle

-- spawn_process: clone the handle and pass it along
assert(spawn_process({
    bin = "/bin/echo.lua",
    stdout = f,
    args = {
        "test",
    },
}))

assert(f:close()) -- close our file handle, the other process still has a clone opened
```

When a new process inherits a handle it will clone it.

Ever handle still open (and owned) by a process when it exists will be closed automatically.

Handles can be created in the kernel using `sandbox.handle.new(obj: object, proc: process)`, obj needs to implement reference counting using `obj:open() -> obj` and `obj:close() -> boolean`.

#### File Handles

These are wrapped kernel buffer objects.

Supported Operations:
```Lua
file2 = file:open() -- clone this handle
file:close() -- close this handle

file:seek()
file:read()
file:write()
file:remaining()
file:wait() -- wait for available data for file:read()
file:setvbuf()
```

TODO:
- [ ] decide on how to handle timeouts consitently
- [ ] decide on how to handle `<file>:read(0)`
- [ ] decide on how to implement setvbuf (also in relation to the TTY)

## Kernel Objects

### Streams

```Lua
-- stream
-- created by: vfs.open
-- consumed by: buffer.new

stream:close(): nil or boolean
stream:seek(whence: string, offset:number): integer
stream:write(value: string): boolean
stream:read(count: number): integer or nil

stream:remaining(): integer or nil -- does not have to be supported, check before calling

stream.__wait: obj -- optional
stream:get_wait(): obj -- optional
```

### File / Buffer

```Lua
-- buffer
-- created by: buffer.new

-- Reference Counting
buffer:close(): nil or boolean
buffer:open(): self or nil

--
buffer:remaining(): integer or nil
buffer:flush(): boolean
buffer:read(...): values
buffer:seek(whence: string or nil, offset: integer or nil): integer
buffer:setvbuf(mode: string or nil, size: integer or nil): string, integer
buffer:getTimeout(): integer
buffer:setTimeout(value: number)
buffer:write(...): boolean

buffer.__wait: obj -- optional
```

### Socket Handles

```Lua
-- socket
-- created by: net.socket.new()

-- Reference counting
socket:open()
socket:close()

socket:bind()
socket:send()
socket:receive(timeout: number): nil or socket_message
socket:remainig()

socket.__wait: obj
```
