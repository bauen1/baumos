-- bin/edit.lua - simple text editor
-- vim: set tabstop=2 shiftwidth=2 expandtab :
--

local fs = require("filesystem")
local shell = require("shell")

local prog_name, args, _ = shell.parse(...)

if not args[1] then
  print(string.format("usage: %s [file]", prog_name or "edit"))
  return 1
end

local file = args[1]

io.input():setvbuf("no")

local function read(from, to)
  local started, data
  while true do
    local c, err = io.read(1)
    if not c then
      return nil, err
    end

    if not started and c == from then
      started = true
      data = c
    elseif started then
      if c == to then
        return data .. c
      else
        data = data .. c
      end
    end
  end
end

-- FIXME: this is broken (after commit that fixed read(0) probably)
print(io.output().bufferMode)

io.write("\27[999;999H\027[6n")

local code = assert(read("\x1b", "R"))
local h, w = code:match("\x1b%[(%d+);(%d+)R")

-- size of the editor pane
local edith = tonumber(h) - 2
local editw = tonumber(w)

-- location of the cursor
local x, y = 1, 1

-- offset in the current line
local atline = 1

local lines = {}

if fs.exists(file) then
  for line in io.lines(file) do
    lines[#lines + 1] = line
  end
end

local function write_file()
  local out, err = io.open(file, "w")
  if not out then
    return nil, err
  end

  out:write(table.concat(lines, "\n"))
  out:close()

  return true
end

local function io_setcursor(l, c)
  io.write(string.format("\27[%d;%dH", l, c))
end

local function setcur()
  io_setcursor(y - atline + 2, x)
  io.flush()
end

local function render(startline, nlines)
  for n = 1, nlines do
    local l_y = startline - atline + n + 1
    local line = lines[n + startline - 1] or ""
    io.write(string.format("\27[%d;1H\27[K%s", l_y, unicode.sub(line, 1, editw)))
  end
  setcur()
end

local msg = ""

local function render_status()
  io.write(
    -- move cursor to 0 0
    "\27[0;0H",

    -- set color
    "\27[31;47m",

    -- clear line
    "\27[K",

    -- status message
    string.format("edit %s: %s", file, msg),

    -- move cursor to bottom line
    string.format("\27[%d;0H", h),

    -- set color and clear
    "\27[34;47m",

    -- clear line
    "\27[K",

    -- bottom status line
    "save: f1/ctrl+o save+quit: f2/ctrl+x quit: f3/ctrl+c",

    -- reset color
    "\27[;m"
  )

  setcur()
end

render_status()
render(1, edith)

local run = true

while run do
  local c = io.read(1)
  if c == "\027" then
    local c1 = io.read(1)
    if c1 == "[" then
      local c2 = io.read(1)
      if c2 == "1" then
        if io.read(1) == "~" then -- HOME
          x = unicode.len(lines[y])
          render(y, 1)
          setcur()
        end
      elseif c2 == "4" then
        if io.read(1) == "~" then -- END
        end
      elseif c2 == "A" then -- cursor up
        if y > 1 then
          y = y - 1
          if unicode.len(lines[y]) < x then
            x = unicode.len(lines[y]) + 1
          end
          if y < atline then
            atline = y
            render(y, edith)
          end
          setcur()
        end
      elseif c2 == "B" then -- cursor down
        y = y + 1
        lines[y] = lines[y] or ""
        if unicode.len(lines[y]) < x then
          x = unicode.len(lines[y]) + 1
        end

        if y > atline + edith - 1 then
          atline = y - edith + 1
          render(y - edith + 1, edith)
        end
        setcur()
      elseif c2 == "C" then -- cursor right
        if unicode.len(lines[y]) < x then
          y = y + 1
          x = 1
          lines[y] = lines[y] or ""
          if y > atline + edith - 1 then
            atline = y - edith + 1
            render(y - edith + 1, edith)
          end
          setcur()
        else
          x = x + 1
          setcur()
        end
      elseif c2 == "D" then -- cursor left
        if x - 1 < 1 then
          if y > 1 then
            y = y - 1
            if y < atline then
              atline = y
              render(y, edith)
            end
            x = unicode.len(lines[y]) + 1
            setcur()
          end
        else
          x = x - 1
          setcur()
        end
      end
    elseif c1 == "O" then
      local c2 = io.read(1)
      if c2 == "P" then -- F1
        local success, err = write_file()
        if not success then
          msg = err
          render_status()
        end
      elseif c2 == "Q" then -- F2
        local success, err = write_file()
        if not success then
          msg = err
          render_status()
        else
          run = false
        end
      elseif c2 == "R" then -- F3
        run = false
      elseif c2 == "S" then -- F4
        --
      end
    end
  elseif c == "\n" then
    local line = lines[y]
    lines[y] = unicode.sub(line or "", 1, x - 1)
    table.insert(lines, y + 1, unicode.sub(line or "", x))
    x = 1
    render(y, atline + edith - y -1)
    y = y + 1
    if y > atline + edith - 1 then
      atline = y - edith - 1
      render(y - edith + 1, edith)
    end
    setcur()
  elseif c == "\b" then
    if x > 1 then
      lines[y] = unicode.sub(lines[y] or "", 1, x - 2) .. unicode.sub(lines[y] or "", x)
      x = x - 1
      render(y, 1)
    elseif y > 1 then
      x = unicode.len(lines[y - 1]) + 1
      lines[y - 1] = lines[y - 1] ..lines[y]
      table.remove(lines, y)
      y = y - 1
      render(y, atline + edith - y - 1)
    end
  elseif c == "\3" then -- ctrl+c
    run = false
  elseif c == "\15" then -- ctrl+o
    local success, err = write_file()
    if not success then
       msg = err
    else
      msg = "Written " .. file
    end
    render_status()
  elseif c == "\24" then -- ctrl+x
    local success, err = write_file()
    if not success then
      msg = err
      render_status()
    else
      run = false
    end
  else
    lines[y] = unicode.sub(lines[y] or "", 1, x - 1) .. c .. unicode.sub(lines[y] or "", x)
    render(y, 1)
    x = x + 1
    setcur()
  end
end

-- reset the screen (and clear it)
io.write("\27c")

return 0
