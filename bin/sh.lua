-- bin/sh.lua - shell
-- vim: set tabstop=2 shiftwidth=2 expandtab :
--

if not os.getenv("PATH") then
    os.setenv("PATH", "/bin:/usr/bin:/usr/local/bin")
end

if not os.getenv("USER") then
    os.setenv("USER", "root")
end

local fs = require("filesystem")
local shell = require("shell")
local computer = require("computer")
local libedit = require("libedit")

local sh_name, sh_args, sh_opts = shell.parse(...)
print("name = ", sh_name)
print("args = ", sh_args)
print("options = ", sh_opts)

local run = true

--[[ Builtins ]]
local builtins = {}

builtins.exit = function()
  run = false
end

builtins.echo = function(args)
  for i = 2, #args do
    if i > 2 then
      io.write(" ")
    end
    io.write(args[i])
  end
  io.write("\n")
end

builtins.free = function(args)
  local _ = args
  local free = computer.freeMemory()
  local total = computer.totalMemory()

  local used = total - free
  local used_p = used / total
  local free_p = free / total

  io.write(string.format(
    "%8.2fkb (%8d bytes) %5.1f%% free\n" ..
    "%8.2fkb (%8d bytes) %5.1f%% used\n" ..
    "%8.2fkb (%8d bytes) 100.0%% total\n",
    free / 1024, free, free_p * 100,
    used / 1024, used, used_p * 100,
    total / 1024, total
    ))
end

builtins.env = function(args)
  local _ = args

  local env = os.getenv()
  for k, v in pairs(env) do
    io.write(string.format("%s=%s\n", tostring(k), tostring(v)))
  end
end

builtins.export = function(args)
  for i = 2, #args do
    local name, value = args[i]:match("([^=]-)=(.*)")
    if not value then
        value = ""
    end
    os.setenv(name, value)
  end
end

builtins.cd = function(args)
  local dir = args[2]
  if not dir then
    os.setenv("PWD", os.getenv("HOME") or "/")
  else
    if dir:sub(1, 1) ~= "/" then
      dir = fs.concat(os.getenv("PWD") or "/", dir)
    end
    if not fs.isDirectory(dir) then
      io.write(string.format("cd: %s: not a directory\n", dir))
      return false
    end
    os.setenv("PWD", dir)
  end

  return true
end

builtins.reset = function()
  io.write("\27c");
  io.flush()
end

builtins.clear = builtins.reset

--[[ helper functions ]]
local function expand(value)
  -- replace all occurences of $VAR and ${VAR} with the environmental value of VAR
  -- it also respects $$ and \$
  return (value:gsub("%$(%w+)", os.getenv):gsub("%${(%w+)}", os.getenv):gsub("[%$\\]%$", "$"))
end

local function tokenize(value)
  local tokens = {}
  local token = ""
  local quoted = false
  local escaped = false
  local start = -1

  for i = 1, unicode.len(value) do
    local char = unicode.sub(value, i, i)
    if escaped then
      escaped = false
      token = token .. char
    elseif char == "\\" and quoted ~= "'" then
      escaped = true
      token = token .. char
    elseif char == quoted then
      quoted = false
      token = token .. char
    elseif (char == "'" or char == '"' or char == '`') and not quoted then
      quoted = char
      start = i
      token = token .. char
    elseif string.find(char, "%s") and not quoted then -- delimiter
      if token ~= "" then
        table.insert(tokens, token)
        token = ""
      end
    else -- normal char
      token = token .. char
    end
  end
  if quoted then
    return nil, "unclosed quote at index " .. start, quoted
  end
  if token ~= "" then
    table.insert(tokens, token)
  end
  return tokens
end

-- find the real path of a program by name in $PATH
local function resolveProgram(name)
  if name:sub(1, 1) == "/" or name:sub(1, 2) == "./" then
    if fs.exists(name) then
      return name
    else
      return nil
    end
  end

  for dir in string.gmatch(os.getenv("PATH") or "/bin", "[^:$]+") do
    if dir:sub(1, 1) ~= "/" then
      dir = fs.concat(os.getenv("PWD") or "/", dir)
    end

    local file = fs.concat(dir, name .. ".lua")
    if fs.exists(file) then
      return file
    end

    file = fs.concat(dir, name)
    if fs.exists(file) then
      return file
    end
  end

  return nil
end

local function execute(cmd)
  local tokens, err = tokenize(cmd)

  if not tokens then
    io.write("error: " .. tostring(err) .. "\n")
    return
  end

  -- parse
  for k, token in ipairs(tokens) do
    if token:match("[%w%._%-/-]+") then
      local tlen = #token
      if (token:sub(1,1) == '"' or token:sub(1,1)=="'")
        and (token:sub(tlen,tlen)=='"' or token:sub(tlen,tlen) == "'") then
        -- remove quotes
        tokens[k] = token:sub(2, tlen - 1)
      end
    end

    tokens[k] = expand(tokens[k])
  end

  local program = tokens[1]
  if not program then
    return
  end

  if builtins[program] then
    return builtins[program](tokens)
  end

  local bin = resolveProgram(program)
  if not bin then
    io.write("\27[31;mProgram '" .. tostring(program) .. "' was not found\27[39;m\n")
    return
  end

  local proc, err = os.spawnp(bin, io.input(), io.output(), io.stderr(), table.unpack(tokens, 2))
  if not proc then
    print(string.format("proc error: %s", tostring(err)))
    return nil
  else
    print(string.format("proc.pid = %d", proc.pid))
    return proc:wait()
  end
end

local function get_hints_split(line)
  local tokens, err = tokenize(line)
  if not tokens then
    return nil
  end

  local last_word = tokens[#tokens]
  if not last_word then return nil end

  local prefix = unicode.sub(line, 1, -unicode.len(last_word) - 1)

  if #tokens == 1 then
    return prefix, last_word, nil
  else
    return prefix, nil, last_word
  end
end

local function file_hints(s)
  local name = s:gsub("^.*/", "")

  local basePath = unicode.sub(s, 1, -unicode.len(name) - 1)

  local result, baseName = {}

  if fs.isDirectory(basePath) and name == "" then
    baseName = "^(.-)/?$"
  else
    baseName = "^(" .. name .. ".-)/?$"
  end

  local name_len = unicode.len(name)

  for file in fs.list(basePath) do
    if name == "" or unicode.sub(file, 1, name_len) == name then
      if string.sub(file, -1) == "/" then
        table.insert(result, basePath .. string.sub(file, 1, -2))
      else
        table.insert(result, basePath .. file)
      end
    end
  end

  if #result == 1 and fs.isDirectory(result[1]) then
    result[1] = result[1] .. "/"
  end

  return result
end

local function path_hints(s)
  local results = {}
  local function check(key)
    if key:find(s, 1, true) == 1 then
      results[#results + 1] = key
    end
  end
  for builtin in pairs(builtins) do
    check(builtin)
  end

  for dir in string.gmatch(os.getenv("PATH") or "/bin", "[^:$]+") do
    if dir:sub(1, 1) ~= "/" then
      dir = fs.concat(os.getenv("PWD") or "/", dir)
    end

    for file in fs.list(dir) do
      check(file:gsub("%.lua$", ""))
    end
  end

  return results
end

local function get_hints(buf, pos)
  -- everything before the cursor
  local line = unicode.sub(buf, 1, pos - 1)
  -- after the cursor
  local suffix = unicode.sub(buf, pos)

  local prev, cmd, arg = get_hints_split(line)
  if not prev then return nil end

  local results
  if cmd then
    if cmd:find("/") then
      results = file_hints(cmd)
    else
      results = path_hints(cmd)
    end
  elseif arg then
    results = file_hints(arg)
  end

  table.sort(results)
  for i = 1, #results do
    results[i] = {
      prev .. results[i] .. suffix, -- buf
      unicode.len(prev .. results[i]) + 1 -- cursor pos
    }
  end

  return results
end

local hist = {}

while run do
  io.write("\27[49m\27[39m")
  io.write(expand(os.getenv("PS1") or "\27[33m$USER@$HOSTNAME\27[;;m:\27[31m$PWD\27[;;m $$ \27[;;m"))
  io.flush()
  local cmd, err  = libedit.readline(hist, get_hints)
  if not cmd then
    print("err: " .. tostring(err))
  end

  execute(cmd)
end
