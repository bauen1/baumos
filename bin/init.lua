-- bin/init.lua - userspace entrypoint
-- vim: set tabstop=2 shiftwidth=2 expandtab :
--

-- open stdout
if not io.output() then
    local stdout = io.open("/dev/tty", "w")
    stdout:setvbuf("no")
    io.output(stdout)
end

-- open stdin
if not io.input() then
    local stdin = io.open("/dev/tty", "r")
    stdin:setvbuf("line")
    io.input(stdin)
end

-- TODO: open stderr

-- setup default env
local function env_default(name, val)
  if not os.getenv(name) then
    os.setenv(name, val)
  end
end

env_default("PATH", "/bin:/usr/bin:/usr/local/bin")
env_default("PWD", "/")
env_default("HOME", "/root")
env_default("SHELL", "/bin/sh.lua")
env_default("HOSTNAME", "node")
env_default("LS_COLORS", "di=0;36:fi=0:ln=0;33:*.lua=0;32")

-- start a shell and respawn it if it exits
while true do
  print("spawn shell")
  local proc, err = os.spawnp(os.getenv("SHELL"), io.input(), io.output(), io.output())

  if not proc then
    print("error: ", err)
  else
    local success
    success, err = proc:wait()
    if not success then
      print("error: ", err)
    end
  end
  os.sleep(5) -- backoff
end
