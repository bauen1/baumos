-- bin/rm.lua - remove files or directories
-- vim: set tabstop=2 shiftwidth=2 expandtab :
--
local shell = require("shell")

local prog_name, args, opts = shell.parse(...)

if #args == 0 then
  io.write(string.format("usage: %s [-v] <file1> [<file2>, ...]\n -v: verbose output", prog_name))
  return false
end

local r = true
for i = 1, #args do
  local path = args[i]
  local success, err = os.remove(path)

  if not success then
    io.stderr():write(string.format("%s: error: %s\n", path, err))
    r = false
  end

  if opts.v then
    io.write(string.format("remove '%s'\n", path))
  end
end

return r
