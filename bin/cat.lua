-- bin/cat.lua - read files to stdout
-- vim: set tabstop=2 shiftwidth=2 expandtab :
--

local shell = assert(require("shell"))

local prog_name, args, _ = shell.parse(...)
print("prog_name: " .. tostring(prog_name))

if #args == 0 then
  repeat
    local read = io.read()
    if read then
      io.write(read)
    end
  until not read
else
  for i = 1, #args do
    local file, err = io.open(args[i], "rb")
    if not file then
      io.stderr():write(string.format("error opening '%s': %s", args[i], tostring(err)))
      return 1
    end
    local add_newline = false
    repeat
      local l = file:read("*L")
      if l then
        add_newline = string.sub(l, string.len(l), string.len(l)) ~= "\n"
        io.write(l)
      end
    until not l
    file:close()

    if add_newline then
      io.write("\n")
    end

    io.flush()
  end
end
