-- bin/lua.lua - lua repl
-- vim: set tabstop=2 shiftwidth=2 expandtab :
--

local libedit = require("libedit")

local hist = {}

while true do
  io.write("> ")
  local l = libedit.readline(hist)
  if not l then -- eof
      return
  end

  local f, err
  f, err = load("return " .. l, "=stdin", "t")
  if not f then
    f, err = load(l, "=stdin", "t")
  end

  if f then
    local result = table.pack(xpcall(f, debug.traceback))
    if not result[1] then
      io.output():write(tostring(result[2]) .. "\n")
    else
      local ok, why = pcall(function()
       for i = 2, result.n do
         io.write(require("serialization").serialize(result[i], true) .. "\t")
       end
      end)
      if not ok then
        io.output():write("crashed serializing result: ", tostring(why))
      end
      io.write("\n")
    end
  else
    io.output():write(tostring(err) .. "\n")
  end
end
