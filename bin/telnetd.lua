-- bin/telnetd.lua - modem test
-- vim: set tabstop=2 shiftwidth=2 expandtab :
--
local net = require("net")
local buffer = require("buffer")

local modem = assert(net.list_modems()())
local socket = assert(net.socket.open(modem))

assert(socket:bind(2))

local stdin_out, stdin_in = buffer.pipe()
local stdout_out, stdout_in = buffer.pipe()

stdin_out:setvbuf("no")

local pid = assert(os.spawnp("/bin/sh.lua", stdin_out, stdout_in, stdout_in))
print("pid = ", pid)
stdin_out:close() -- close our clones
stdin_out = nil -- luacheck: ignore
stdout_in:close()
stdout_in = nil -- luacheck: ignore

stdout_out:setvbuf("no")
stdin_in:setvbuf("no")

-- FIXME: this breaks stuff
--os.sleep(0)

while true do
  local v, err = os.wait(math.huge, socket, stdout_out)
  if not v then
    print("error: " .. tostring(err))
    break
  end

  while socket:remaining() > 0 do
    local daddr, saddr, _, _, msg1 = socket:receive(0)
    if not daddr then
      print("error: " .. tostring(saddr))
      break
    end

    --print("<< " .. msg1)
    stdin_in:write(msg1)
  end

  while stdout_out:remaining() > 0 do
    local v, err = stdout_out:read(0)
    if not v then
      print("error: " .. tostring(err))
      break
    end

    --print(">> " .. msg)

    socket:send(nil, 2, v)
  end
end

assert(socket:close())
