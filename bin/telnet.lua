-- bin/telnet.lua - modem test client
-- vim: set tabstop=2 shiftwidth=2 expandtab :
--

local net = require "net"

local modem = assert(net.list_modems()())

local socket = assert(net.socket.open(modem))

print("bind socket to port 2")
assert(socket:bind(2))

while true do
  local v, err = os.wait(math.huge, socket, io.input())
  if not v then
    print("error: " .. tostring(err))
    break
  end

  if v == socket then
    --print("wait => socket")
    local daddr, saddr, _, _, msg1 = socket:receive(0)
    if not daddr then
      print("error: " .. tostring(saddr))
      break
    end

    io.write(tostring(msg1))
  elseif v == io.input() then
    --print("wait => input")
    v, err = io.read(0)
    if not v then
      print("error: " .. tostring(err))
    end
    socket:send(nil, 2, v)
  end
end

assert(socket:close())
