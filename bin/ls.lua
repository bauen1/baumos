-- bin/ls.lua - list directory contents
-- vim: set tabstop=2 shiftwidth=2 expandtab :
--
local fs = assert(require("filesystem"))
local shell = require("shell")

local function usage()
  print([[Usage: ls [OPTION]... [FILE]...
  -a, --all            do not ignore entries starting with .
      --full-time      with -l print time in full iso format
  -h, --human-readable with -l print human readable sizes
      --si             like -h but use powers of 1000
  -l                   use a long listing format
  -R, --recursive      list subdirectories recursively
  -1                   list one file per line
      --no-color       don't use colors
      --help           this help]])
end

local prog_name, args, options = shell.parse(...)

if options.help then
  return usage()
end

local set_color
local colorize

-- TODO: check if tty
if not options["no-color"] then
  local LS_COLORS = os.getenv("LS_COLORS") or ""

  local color_lookup = {}
  for k, v in string.gmatch(LS_COLORS, "([^:=]*)=([^:=]*):?") do
    color_lookup[k] = v
  end

  colorize = function(info)
    return
      -- info.err and color_lookup["err"] or
      info.isLink and color_lookup["ln"] or
      info.isDir and color_lookup["di"] or
      color_lookup["*" .. info.ext] or
      color_lookup["fi"]
  end

  set_color = function(c)
    io.write(string.format("\027[%sm", c or ""))
  end
else
  set_color = function() end
  colorize = function() end
end

local function formatSize(size)
  -- TODO: implement
  return tostring(size)
end

local function display(infos)
  -- TODO: implement
end

local function stat(path, name)
  local info = {}

  function check(value, err)
    if not value then
      info.err = err
    end
    return value
  end

  info.key = name
  info.path = name and name:sub(1, 1) == "/" and "" or path
  info.full_path = check(fs.concat(info.path, name))
  info.isDir = check(fs.isDirectory(info.full_path))
  info.name = name and (name:gsub("/+$", "") .. (info.isDir and "/" or "")) or path
  info.sort_name = info.name:gsub("^%.", "")
  info.isLink, info.link = check(fs.isLink(info.full_path))
  info.size = info.isLink and 0 or check(fs.size(info.full_path))
  info.time = check(fs.lastModified(info.full_path))
  info.ext = info.name:match("(%.[^.]+)$") or ""
  return info
end

local function listPath(path, name)
  local info = stat(path, name)
  set_color(colorize(info))
  print(info.name or info.path)
  set_color()
end

local dirs = args
if dirs.n == 0 then
  table.insert(dirs, ".")
  dirs.n = 1
end

for i = 1, dirs.n do
  local dir = dirs[i]
  listPath(dir)
  for v in fs.list(dir) do
    listPath(dir, v)
  end
end
io.output():flush()
