-- sys/tty.lua - screen/gpu driver
-- vim: set tabstop=2 shiftwidth=2 expandtab :

---
-- @classmod tty

local tty = {}
local metatable = {
  __index = tty,
}

-- calculate gpu color from ansi grayscale index
local function get_grayscale(n)
  return math.floor(0xffffff * (n - 232) / 23)
end

local function clamp_cursor(x, y, w, h)
  return math.min(math.max(x, 1), w), math.min(math.max(y, 1), h)
end

-- lookup table for ANSI colors
local colors_lookup = {
  0x0,
  0xff0000,
  0x00ff00,
  0xffff00,
  0x0000ff,
  0xff00ff,
  0x00B6ff,
  0xffffff
}

---
-- @tparam component.gpu gpu
-- @tparam component.screen screen
-- @treturn tty self
function tty.new(gpu, screen)
  local self = setmetatable({
    gpu = gpu,
    screen = screen,
    x = 0,
    y = 0,
    buf = "",
    __type = "tty",
  }, metatable)

  self.w, self.h = gpu.maxResolution()
  gpu.setResolution(self.w, self.h)
  self:reset()
  gpu.fill(1, 1, self.w, self.h, ' ')
  self:cursor_blink()

  self:update_keyboards()

  local map = {
    touch = self.screen_touch,
    drag = self.screen_drag,
    drop = self.screen_drop,
    screen_resized = self.screen_resized,
    key_down = self.handle_keyboard_event,
    key_up = self.handle_keyboard_event,
    clipboard = self.handle_keyboard_event,

    -- Keep self.keyboards updated
    component_added = self.update_keyboards,
    component_removed = self.update_keyboards,
  }

  kevent.listen("signal", function(_, signal, ...)
    if map[signal] then
      return map[signal](self, signal, ...)
    end
  end)

  local function invalid()
    return nil, "tty: invalid operation"
  end

  self.devfs_file = {
    __type = "file",
    __wait = self,
    write = function(_, str)
      return self:write(str)
    end,
    read = function(_, ...)
      return self:read(...)
    end,
    seek = invalid,
    close = invalid,
    remaining = function(_)
      return self:remaining()
    end,
    set_echo = function(_, value)
      self.echo = not not value
    end,
    get_echo = function(_)
      return self.echo
    end,
    istty = function(_)
      return true
    end,
  }

  return self
end

function tty:update_keyboards()
  self.keyboards = {}

  for _, v in pairs(self.screen.getKeyboards()) do
    self.keyboards[v] = {
      pressedCodes = {},
      pressedChars = {},
    }
  end
end

function tty:keyboard_is_key_down(keyboard, charOrCode)
  local _ = self
  checkArg(1, charOrCode, "string", "number")
  if type(charOrCode) == "string" then
    return keyboard.pressedChars[utf8 and utf8.codepoint(charOrCode) or string.byte(charOrCode)]
  elseif type(charOrCode) == "number" then
    return keyboard.pressedCodes[charOrCode]
  end
end

function tty:keyboard_is_ctrl_down(keyboard)
  local _ = self
  return keyboard.pressedCodes[keymap.keys.lcontrol] or keyboard.pressedCodes[keymap.keys.rcontrol]
end

function tty:keyboard_is_alt_down(keyboard)
  local _ = self
  return keyboard.pressedCodes[keymap.keys.lmenu] or keyboard.pressedCodes[keymap.keys.rmenu]
end

function tty:keyboard_is_shift_down(keyboard)
  local _ = self
  return keyboard.pressedCodes[keymap.keys.lshift] or keyboard.pressedCodes[keymap.keys.rshift]
end

function tty:__blink_cursor(x, y)
  if x < 1 or x > self.w or y < 1 or y > self.w then
    -- XXX: this right here is the solution to: What do we do if the
    -- cursor is at the end of the screen
    return false
  end

  local old_bg, old_bg_pal = self.gpu.getBackground()
  local old_fg, old_fg_pal = self.gpu.getForeground()
  local char, fg, bg, fg_pal, bg_pal = self.gpu.get(x, y)
  self.gpu.setBackground(fg_pal or fg, not not fg_pal)
  self.gpu.setForeground(bg_pal or bg, not not bg_pal)
  self.gpu.set(x, y, char or " ")
  self.gpu.setBackground(old_bg_pal or old_bg, not not old_bg_pal)
  self.gpu.setForeground(old_fg_pal or old_fg, not not old_fg_pal)

  return true
end

function tty:cursor_unblink()
  if self.cursor_visible then
    if self:__blink_cursor(self.x, self.y) then
      self.cursor_visible = false
    end
  end
end

function tty:cursor_blink()
  assert(not self.cursor_visible)
  if self.show_cursor then
    self.cursor_visible = self:__blink_cursor(self.x, self.y)
  end
end

function tty:scroll(lines)
  if not lines then
    if self.y < 1 then
      lines = self.y - 1 -- y == 0 scrolls back -1
    elseif self.y > self.h then
      lines = self.y - self.h -- y == height + 1 scroll forward 1
    else
      return 0 -- do nothing
    end
  end

  -- clamp to [-h .. h]
  lines = math.max(math.min(lines, self.h), -self.h)

  local abs_lines = math.abs(lines)
  local box_height = self.h - abs_lines
  local fill_top = 1 + (lines < 0 and 0 or box_height)
  self.gpu.copy(1, 1 + math.max(0, lines), self.w, box_height, 0, -lines)
  self.gpu.fill(1, fill_top, self.w, abs_lines, ' ')

  self.x = self.x
  self.y = math.max(1, math.min(self.y, self.h))
  return lines
end

--- VT100 ANSI control code implementation
-- @tparam string str
-- @tparam integer x
-- @tparam integer y
function tty:ansi(str, x, y)
  local c = string.sub(str, 1, 1)
  if c == "c" then
    -- Full reset
    x = 1
    y = 1
    self:reset()
    self.gpu.fill(1, 1, self.w, self.h, " ")

    return 2, x, y
  elseif c == "7" then
    -- save attribute
    -- FIXME: \ESC[s
    self.saved = {
      x,
      y,
      { self.gpu.getBackground() },
      { self.gpu.getForeground() },
      self.flip,
      self.blink,
    }

    return 2, x, y
  elseif c == "8" then
    -- restore attribute
    local data = self.saved or {1,1,{0x0},{0xffffff}}
    x = data[1]
    y = data[2]
    self.gpu.setBackground(table.unpack(data[3]))
    self.gpu.setForeground(table.unpack(data[4]))
    self.flip = data[5]
    self.blink = data[6]

    return 2, x, y
  elseif c == "D" then
    -- FIXME: scroll
    y = y + 1
    return 2, x, y
  elseif c == "E" then
    -- FIXME: scroll
    y = y + 1
    x = 1
    return 2, x, y
  elseif c == "M" then
    -- FIXME: scroll
    y = y - 1
    return 2, x, y
  elseif c == "[" then
    -- TODO: this can be made more efficient by not creating a new table
    -- including scoped functions every time this is run
    -- TODO: Stop using scoped functions, they are probably the worst thing for performance here
    local rules = {
      ["^(([%d;]*)m)"] = function(_, color)
        local setFG = self.gpu.setForeground
        local setBG = self.gpu.setBackground

        local flip = self.flip
        if flip then
          setFG, setBG = setBG, setFG
        end

        local next_part = color:gmatch("(%d+);?")

        local function do_reset()
          setBG(colors_lookup[1])
          setFG(colors_lookup[8])
        end

        local function get_color()
          local n1 = tonumber(next_part())

          if n1 == 5 then
            local n2 = tonumber(next_part())
            if n2 < 8 then
              return colors_lookup[n2]
            elseif n2 >= 232 and n2 <= 255 then
              return get_grayscale(n2)
            end
          elseif n1 == 2 then
            local r = tonumber(next_part())
            local g = tonumber(next_part())
            local b = tonumber(next_part())

            if r and g and b then
              return r * 256 * 256 + g * 256 + b
            end
          end

          return -- invalid
        end

        if string.sub(color, 1, 1) == ";" then
          do_reset()
        end

        local first_pass
        while true do
          local s_num = next_part()

          local num = tonumber(s_num)

          if not num then break end
          first_pass = true

          if num == 0 then
            do_reset()
          elseif num == 7 then -- flip
            if not flip then
              local rgb, pal = self.gpu.setBackground(self.gpu.getForeground())
              self.gpu.setForeground(pal or rgb, not not pal)
              setFG, setBG = setBG, setFG
              flip = true
            end
          elseif num == 27 then -- no flip
            if flip then
              local rgb, pal = self.gpu.setBackground(self.gpu.getForeground())
              self.gpu.setForeground(pal or rgb, not not pal)
              setFG, setBG = setFG, setBG
              flip = false
            end
          elseif num >= 30 and num <= 37 then
            local col = colors_lookup[num - 29]
            if col then
              setFG(col)
            end
          elseif num >= 40 and num <= 47 then
            local col = colors_lookup[num - 39]
            if col then
              setBG(col)
            end
          elseif num == 38 then
            local col = get_color()
            if col then
              setFG(col)
            end
          elseif num == 48 then
            local col = get_color()
            if col then
              setBG(col)
            end
          else -- luacheck: ignore
            -- invalid escape code
          end
        end

        if string.sub(color, -1) == ";" or not first_pass then
          do_reset()
        end
        self.flip = flip
      end,
      ["^((%d*)([ABCD]))"] = function(_, n, l)
        -- move cursor
        n = tonumber(n) or 1
        local dy = (l == "A" and -n) or (l == "B" and n) or 0
        local dx = (l == "D" and -n) or (l == "C" and n) or 0
        x, y = clamp_cursor(x + dx, y + dy, self.w, self.h)
      end,
      ["^((%d*);(%d*)[Hf])"] = function(_, line, column)
        -- [Line;ColumnH move cursor to position
        -- [Line;Columnf ^
        x, y = clamp_cursor(tonumber(column) or 1, tonumber(line) or 1, self.w, self.h)
      end,
      ["^(([012]?)K)"] = function(_, n)
        n = tonumber(n) or 0
        local _x = (n == 0 and x or 1) -- XXX: x instead of self.x
        local rep = (n == 1 and _x or (self.w - _x + 1))
        self.gpu.fill(_x, y, rep, 1, " ")
      end,
      ["^(([012]?)J)"] = function(_, n)
        n = tonumber(n) or 0
        -- Repetition of above
        local _x = (n == 0 and x or 1)
        local _rep = (n == 1 and x or (self.w - x + 1))
        self.gpu.fill(_x, y, _rep, 1, " ")
        local _y = (n == 0 and (y + 1)) or 1
        local _rep2 = (n == 1 and (y - 1)) or self.h
        self.gpu.fill(1, _y, self.w, _rep2, " ")
      end,
      ["^(6n)"] = function(_)
        -- report current cursor position
        self.buf = self.buf .. string.format("\27[%d;%dR", math.floor(self.y), math.floor(self.x))

        process.notify(self.buf)
      end,
      ["^(%?([%d;]+)([hl]))"] = function (_, codes, action)
        -- action 'h' => set
        -- action 'l' => unset
        action = action == 'h'
        local next_code = codes:gmatch("(%d+);?")
        while true do
          local code = next_code()
          if not code then break end
          code = tonumber(code)
          if code == 1000 then
            self.mouse_mode[1000] = action
          elseif code == 1002 then
            self.mouse_mode[1002] = action
          elseif code == 1003 then
            self.mouse_mode[1003] = action
          elseif code == 1006 then
            self.mouse_mode[1006] = action
          end
        end
      end,
      ["^(12([hl]))"] = function(_, v)
        -- start blinking cursor
        self.blink = (v == "h")
      end,
      ["^(%?25l)"] = function(_)
        -- hide cursor
        self.show_cursor = false
      end,
      ["^(%?25h)"] = function(_)
        -- show cursor
        self.show_cursor = true
      end,
    }

    local sstr = unicode.sub(str, 2)
    for pattern, f in pairs(rules) do
      local result = table.pack(string.match(sstr, pattern))
      if result[1] then
        f(table.unpack(result))

        return unicode.len(result[1]) + 2, x, y
      end
    end
  end

  -- nothing matched replace \027 with ^[
  return 0, x, y, "^["
end

function tty:write(str)
  checkArg(1, str, "string")

  self:cursor_unblink()

  local beeped
  while true do

    -- scroll if necessary
    self:scroll()

    local x = self.x
    local y = self.y

    -- we're done
    if #str == 0 then break end

    -- Now we find the next magic character
    local _, end_index, delimiter = unicode.sub(str, 1, self.w):find("([\27\t\r\n\a\b\v\15])")

    -- find everything before that that can be outputted directly
    local segment = (end_index and str:sub(1, end_index - 1)) or str

    -- print segment (including unicode)
    if segment ~= "" then
      local wlen_needed = unicode.wlen(segment)
      local wlen_remaining = self.w - x + 1

      if wlen_remaining < wlen_needed then
        -- We will reach the end of the screen if we print this
        -- so we only print what will fit
        segment = unicode.wtrunc(segment, wlen_remaining + 1)
        wlen_needed = unicode.wlen(segment)

        if not self.nowrap then
          -- If we should wrap then insert a fake newline
          end_index = string.len(segment)
          delimiter = "\n"
        end
      end

      self.gpu.set(x, y, segment)
      x = x + wlen_needed
    end

    -- remove the segment we just outputted and the magic character
    -- if we have no magic character, also trim to window with (using unicode)
    -- XXX: FIXME: why trim to the window
    str = end_index and str:sub(end_index + 1) or unicode.sub(str, self.w + 1)

    if delimiter == '\t' then     -- tab
      -- Align cursor on 8 characters
      x = ((x - 1) - ((x - 1) % 8)) + 9
    elseif delimiter == '\r' then -- carriage return
      x = 1
    elseif delimiter == '\n' then -- newline
      x = 1
      y = y + 1
    elseif delimiter == '\b' then -- backspace
      x = x - 1
    elseif delimiter == '\v' then -- vertical
      y = y + 1
    elseif delimiter == '\a' then -- bell
      if not beeped then
        -- FIXME: if a process has no access to computer.beep it shouldn't be able to beep
        computer.beep()
        beeped = true
      end
    elseif delimiter == '\27' then -- ESC: ANSI control sequence
      local consumed, pre
      consumed, x, y, pre = self:ansi(str, x, y)

      if pre then
        str = pre .. str:sub(consumed)
      else
        str = str:sub(consumed)
      end
    end

    self.x = x
    self.y = y
  end

  self:cursor_blink()

  return true
end

local key_magic_lookup = {
  -- lookup table for key codes that will be send regardless of keyboard mode
  -- see: https://invisible-island.net/xterm/ctlseqs/ctlseqs.html
  [keymap.keys.home]     = "\027[1~",
  [keymap.keys.insert]   = "\027[2~",
  [keymap.keys.delete]   = "\127", -- = "\027[3~",
  [keymap.keys["end"]]   = "\027[4~",
  [keymap.keys.pageUp]   = "\027[5~",
  [keymap.keys.pageDown] = "\027[6~",
  [keymap.keys.up]       = "\027[A",
  [keymap.keys.down]     = "\027[B",
  [keymap.keys.left]     = "\027[D",
  [keymap.keys.right]    = "\027[C",

  [keymap.keys.f1]       = "\027OP",
  [keymap.keys.f2]       = "\027OQ",
  [keymap.keys.f3]       = "\027OR",
  [keymap.keys.f4]       = "\027OS",
}

function tty:handle_key_down(keyboard, char, code)

  keyboard.pressedChars[char] = true
  keyboard.pressedCodes[code] = true

  if char == 0 then
    -- special key

    local magic = key_magic_lookup[code]

    if magic then
      if self.echo then
        self:write(magic)
      end

      self.buf = self.buf .. magic
    elseif code == keymap.lcontrol then
      -- handled
    else
      klog("unknown code '%d'", code)
    end
  else
    if char == 13 then
      char = 10
    end

    local c = unicode.char(char)

    if self.echo then
      self:write(c)
    end
    self.buf = self.buf .. c
  end
end

function tty:handle_key_up(keyboard, char, code)
  local _ = self

  keyboard.pressedChars[char] = nil
  keyboard.pressedCodes[code] = nil

  return
end

function tty:handle_clipboard(keyboard, content, player)
  local _, _, _, _ = self, keyboard, content, player
  klog("TODO: implement tty:handle_clipboard")
  return
end

function tty:handle_keyboard_event(name, component_address, ...)
  local keyboard = self.keyboards[component_address]

  if not keyboard then
    return
  end

  if name == "key_down" then
    self:handle_key_down(keyboard, ...)
  elseif name == "key_up" then
    self:handle_key_up(keyboard, ...)
  elseif name == "clipboard" then
    self:handle_clipboard(keyboard, ...)
  else
    klog("unknown event received: %s", tostring(name))
  end

  -- wake up anything blocking on self:read(...)
  if #self.buf > 0 then
    process.notify(self)
  end
end

function tty:remaining()
  return #self.buf
end

function tty:read(n)
  n = math.tointeger(n) or 0

  if n < 0 then
    return nil, "invalid operation"
  end

  -- this is basically a simpler implementation of buffer:read
  local s = ""

  if n == 0 then
    s = self.buf
    self.buf = ""
    return s
  end

  if string.len(s) < n then
    -- XXX: we only poll once
    while string.len(self.buf) == 0 do
      local obj, err = process.wait(math.huge, self)
      if not obj then
        -- XXX: we don't want to discard anything
        return s, err
      elseif obj == self then
        break
      end
    end

    assert(string.len(self.buf) > 0)

    s = s .. string.sub(self.buf, 1, n)
    self.buf = string.sub(self.buf, n + 1)
  end

  return s
end

function tty:screen_touch(_, component_addr, x, y, mode)
  if component_addr ~= self.screen.address then
    return
  end

  if self.mouse_mode[1000] then
    if self.mouse_mode[1006] then
      -- SGR
      local s = string.format("\027[<%d;%d;%dM\027[<%d;%d;%dm", mode, x, y, mode, x, y)
      self:write(s)
      self.buf = self.buf .. s
    end
  end
end

function tty:screen_drag(_, component_addr, x, y, mode)
  if component_addr ~= self.screen.address then
    return
  end

  if self.mouse_mode[1000] and (self.mouse_mode[1002] or self.mouse_mode[1003]) then
    if self.mouse_mode[1006] then
      -- FIXME: is this correct ?
      local s = string.format("\027[<%d;%d;%dM", mode + 32, x, y)
      self:write(s)
      self.buf = self.buf .. s
    end
  end
end

function tty:screen_drop(_, component_addr, x, y, mode)
  if component_addr ~= self.screen.address then
    return
  end

  if self.mouse_mode[1000] and (self.mouse_mode[1002] or self.mouse_mode[1003])then
    if self.mouse_mode[1006] then
      local s = string.format("\027[<%d;%d;%dm", mode + 32, x, y)
      self:write(s)
      self.buf = self.buf .. s
    end
  end
end

function tty:screen_resized(_, component_addr, w, h)
  if component_addr ~= self.screen.address then
    return
  end

  local oldw, oldh = self.w, self.h
  self.w = w
  self.h = h

  if w > oldw then
    -- clear new region
    self.gpu.fill(oldw + 1, 1, w - oldw, h, " ")
  end

  if h > oldh then
    -- clear new region
    self.gpu.fill(1, oldh + 1, w, h - oldh, " ")
  end
end

function tty:reset()
  self.gpu.setBackground(0)
  self.gpu.setForeground(0xffffff)
  self.flip = false
  self.blink = false
  self.mouse_mode = {}
  self.echo = false
  self.show_cursor = true
  self.x = 1
  self.y = 1
end

return tty
