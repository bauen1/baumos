-- sys/sandbox.lua - sandbox magic
-- vim: set tabstop=2 shiftwidth=2 expandtab :
--

local sandbox = {}

sandbox.__type_magic = { "magic" } -- userspace should never ever get a reference to this

function sandbox.wrap_type(obj)
  return setmetatable({}, {
    __index = obj,
    __newindex = obj, -- FIXME: not the safest bet to do this
    __magic = sandbox.__type_magic,
    __type = obj.__type,
  })
end

function sandbox.unwrap_type(obj)
  local mt = getmetatable(obj)
  if not mt then return nil end
  if not rawequal(rawget(mt, "__magic"), sandbox.__type_magic) then return nil end

  return mt.__index, mt
end

sandbox.handle = {}

-- XXX: this assumes that obj was already open() once
function sandbox.handle.new(obj, proc)
  checkArg(1, obj, "table")
  checkArg(2, proc, "table")

  local mt = {
    __real = obj,
    __index = function(_, k)
      local v = obj[k]
      local v_t = type(v)

      --klog("__index[%s]: %s => %s", tostring(t), tostring(k), tostring(v))
      if v_t == "function" then
        -- Wrap all calls to handle:<func>(...) to obj:<func>(...)
        return function(_, ...)
          return v(obj, ...)
        end
      elseif v_t ~= "table" then
        return v
      else
        return nil
      end
    end,
    __newindex = function(_, _, _)
      -- Prevent any write access to any fields of obj
      --klog("__newindex[%s]: k=%s -> v=%s", tostring(t), tostring(k), tostring(v))
      error("permission denied")
    end,
    __magic = sandbox.__type_magic,
    __type = obj.__type,
    __ref = 1,
  }

  local handle
  handle = {
    open = function()
      local __ref = mt.__ref
      assert(__ref, "handle closed")
      mt.__ref = __ref + 1
      return handle
    end,
    close = function()
      local __ref = mt.__ref
      assert(__ref, "double close")
      mt.__ref = __ref - 1
      if __ref == 1 then
        mt.__ref = nil
        -- So basically race conditions are possible here (leaving the ressource open)
        proc.cleanup_list[handle] = nil
        return obj:close()
      end
    end
  }

  proc.cleanup_list[handle] = sandbox.handle.cleanup

  return setmetatable(handle, mt)
end

function sandbox.handle.cleanup(handle)
  local obj, mt = sandbox.handle.unwrap(handle)
  if not obj then return false end
  if not mt then return false end

  mt.__ref = nil
  return obj:close()
end

function sandbox.handle.unwrap(handle)
  local mt = getmetatable(handle)
  if not mt then return nil end
  if not rawequal(rawget(mt, "__magic"), sandbox.__type_magic) then return nil end

  return mt.__real, mt
end

function sandbox.handle.is_open(handle)
  local obj, mt = sandbox.handle.unwrap(handle)
  if not obj then return nil end
  if not mt then return nil end

  return mt.__ref and mt.__ref > 0
end

function sandbox.handle.clone(handle, proc)
  local obj, mt = sandbox.handle.unwrap(handle)
  if not obj then return nil end
  if not mt then return nil end

  if not mt.__ref then return nil, "handle closed" end

  local obj2, err = obj:open()
  if not obj2 then return nil, err end

  return sandbox.handle.new(obj2, proc)
end

function sandbox.getmetatable(v)
  local mt = getmetatable(v)
  if not mt then
    return nil
  end

  if rawequal(rawget(mt, "__magic"), sandbox.__type_magic) then
    return mt.__type
  end

  return mt
end

function sandbox.type(v)
  local t = type(v)
  if t == "table" then
    local mt = getmetatable(v)

    if mt then
      if rawequal(rawget(mt, "__magic"), sandbox.__type_magic) then
        return mt.__type
      end
    end
  end

  return t
end

function sandbox.checkArg(n, have, ...)
  have = sandbox.type(have)

  local function check(want, ...)
    if not want then
      return false
    else
      return have == want or check(...)
    end
  end

  if not check(...) then
    local msg = string.format("bad argument #%d (%s expected, got %s)",
      n, table.concat({...}, " or "), have)
    error(msg, 3)
  end
end

return sandbox
