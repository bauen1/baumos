-- sys/dmesg.lua - kernel logging and early boot console
-- vim: set tabstop=2 shiftwidth=2 expandtab :
--

local component = component

--- kernel logging
-- @module dmesg
local dmesg = {
  buffer = {
    first = 1,
    n = 0,
    limit = 30,
  }
}

--- display information on the console (and insert it into the log)
-- input is passed to string.format
function dmesg.log(...)
  local msg = string.format(...)
  local time = computer.uptime()
  dmesg.buffer.n = dmesg.buffer.n + 1
  dmesg.buffer[dmesg.buffer.n] = { time, msg }
  if dmesg.buffer.n > dmesg.buffer.limit then
    dmesg.buffer[dmesg.buffer.first] = nil
    dmesg.buffer.first = dmesg.buffer.first + 1
  end
  if dmesg.__status then
    dmesg.__status(string.format("kern: [%.8f]: %s\n", time, msg))
  end
  pcall(function()
    local sandbox_addr = component.list("sandbox")()
    if sandbox_addr then
      component.invoke(sandbox_addr, "log", string.format("kern: [%.8f]: %s", time, msg))
    end
  end)
end

--- if a sandbox component is present log this information
-- input is passed to string.format
function dmesg.debug(...)
  local msg = string.format(...)
  local time = computer.uptime()
  dmesg.buffer.n = dmesg.buffer.n + 1
  dmesg.buffer[dmesg.buffer.n] = { time, msg }
  if dmesg.buffer.n > dmesg.buffer.limit then
    dmesg.buffer[dmesg.buffer.first] = nil
    dmesg.buffer.first = dmesg.buffer.first + 1
  end
  pcall(function()
    local sandbox_addr = component.list("sandbox")()
    if sandbox_addr then
      component.invoke(sandbox_addr, "log", string.format("kern: [%.8f]: %s", time, msg))
    end
  end)
end

return dmesg
