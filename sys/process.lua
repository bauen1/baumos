-- sys/process.lua - process implementation
-- vim: set tabstop=2 shiftwidth=2 expandtab :

--- user process implementation
-- @module process

local process = {}
local process_metatable = {}
process_metatable.__index = process_metatable

---
-- @field[type=process] current process: nil = kernel
process.cproc = nil

---
-- @field[type=table] list of all active processes
process.list = {}

--- iterate over all resumable processes (and push a timeout value if necessary)
-- @treturn Iterator
function process.iter_resumable_procs()
  local key
  return function()
    while true do
      local proc
      key, proc = next(process.list, key)
      if not proc then return nil end

      local thread = proc.thread
      if thread and coroutine.status(thread) ~= "dead" then
        if proc.deadline <= computer.uptime() then
          if not proc.args then
            -- TODO: Implement this for Ctrl+C
            proc.args = { nil, "timeout" }
          end

          return proc
        end
      end
    end
  end
end

--- exit the current process
function process.exit(...)
  -- TODO: check ... types
  return coroutine.yield("exit", true, ...)
end

---
-- @tparam number time in seconds
function process.sleep(time)
  return coroutine.yield("sleep", tonumber(time) or 0.0)
end

--- Wait for a notify call on one or more objects
-- @tparam number timeout
-- @tparam object ...
function process.wait(timeout, ...)
  checkArg(1, timeout, "number")
  local objs = table.pack(...)
  assert(objs.n >= 1, "have to specify atleast 1 object")

  local obj_lookup = {}
  for i = 1, objs.n do
    local obj = objs[i]
    checkArg(i + 1, obj, "table") -- anything else really and this breaks, since we need references
    obj_lookup[obj] = true
  end

  return coroutine.yield("wait", timeout, obj_lookup)
end

--- notify any process waiting on an object
function process.notify(obj)
  for _, proc in pairs(process.list) do
    if proc.reason == "wait" then
      if proc.filter and proc.filter[obj] then
        proc.args = { obj }
        proc.deadline = 0.0
      end
    end
  end
end

-- FIXME: we might eventually,
-- after a lot of time has passed and the heat death of the univerese is nearing, run out of pids

local firstFreePid = 1
local function nextpid()
  local v = firstFreePid
  assert(not process.list[v])
  firstFreePid = firstFreePid + 1
  assert(firstFreePid ~= 0) -- don't overflow

  return v
end

---
-- @type process

--- main thread
-- @tparam coroutine thread

--- process name (used by the /proc filesystem)
-- @tparam string name

---
-- @tparam[opt] process parent

--- XXX: used internally, can be the return values of this thread
-- @tparam[opt] table args

--- XXX: used internally
-- @tparam number deadline

--- list of objects to close when the process exists
-- @tparam table cleanup_list

--- environment variables
-- @tparam table env

--- time used by this process (according to @os.clock)
-- @tparam number clock

--- reason for last yield
-- @tparam string reason

--- filter for last yield
-- @tparam object filter

--- create a new process
-- @tparam function entrypoint
-- @tparam string name
-- @tparam boolean isthread
-- @tparam[opt] process parent
-- @treturn process self
function process.new(entrypoint, name, isthread, parent, ...)
  checkArg(1, entrypoint, "function")
  checkArg(2, name, "string")
  checkArg(3, isthread, "boolean")
  checkArg(4, parent, "table", "nil")

  local self = {
    name = name,
    parent = parent,
    args = table.pack(...),
    deadline = 0.0, -- resume as soon as possible
    clock = 0.0,
    clock_last = 0.0,
    reason = "arg",
    filter = nil,
    cleanup_list = {},
    __type = "process",
    __ref = 1, -- the first reference is hold by the thread itself
  }
  self.thread = coroutine.create(function(...)
    -- XXX: the yield format for exit:
    -- ("exit", true, { values })
    -- ("exit", false, { err_msg:string, traceback:string })
    -- TODO: check normal return value types
    return "exit", xpcall(function(...)
        return table.pack(entrypoint(...))
      end, function(err)
        return table.pack(tostring(err), debug.traceback("", 2))
    end, ...)
  end)

  self.env = parent and (isthread and parent.env or setmetatable({}, {__index = parent.env})) or {}

  self.pid = nextpid()
  process.list[self.pid] = self
  return setmetatable(self, process_metatable)
end

function process_metatable:__tostring()
  return string.format("<process pid=%d>", self.pid)
end

--- reference counting
-- @treturn process self
function process_metatable:open()
  if not self.__ref then
    return nil, "closed"
  end

  self.__ref = self.__ref + 1
  return self
end

--- reference counting
-- @treturn boolean success
function process_metatable:close()
  assert(self.__ref, "double close")

  self.__ref = self.__ref - 1
  if self.__ref == 0 then
    self.__ref = nil
    -- the instance kept open by ourself needs special handleing (self:kill())
    assert(not self.thread)
    return self:cleanup()
  end

  return true
end

--- Cleanup all remaining open objects
-- @treturn boolean
function process_metatable:cleanup()
  for v, f in pairs(self.cleanup_list) do
    if v and f then
      -- TODO: How do we handle errors ?
      assert(f(v))
    end
  end

  process.list[self.pid] = nil
  -- TODO: return pid

  return true
end

--- kill the coroutine of this process
-- XXX: it is necessary to use this instead of @process:close to destroy the reference
-- to this process held by the thread itself
-- @treturn boolean success
function process_metatable:kill(...)
  -- TODO: checkArg(...)

  local co_status = coroutine.status(self.thread)
  if not (co_status == "dead" or co_status == "suspended") then
    return nil, string.format("cannot kill process (coroutine.status = %s)", co_status)
  end

  -- XXX: this also stops any scheduleing
  self.thread = nil
  self.args = table.pack(...)
  process.notify(self)
  return assert(self:close())
end

--- wait for a process to finish executing
function process_metatable:wait(timeout)
  timeout = timeout or math.huge
  checkArg(1, timeout, "number")
  if self.thread then
    local v, err = process.wait(timeout, self)
    if not v or v ~= self then
      return nil, err
    end

    -- TODO: return values
    return true, table.unpack(self.args)
  else
    return true
  end
end

--- access the process environment
-- @tparam {nil,number,string} varname
-- @treturn (nil,number,string)
function process_metatable:getenv(varname)
  checkArg(1, varname, "nil", "number", "string")
  if not varname then
    -- clone process environment (including parent) recursively
    -- we don't want to hand out the original to the user
    -- self.env should never be recursive

    local function f(e, t)
      for k, v in pairs(e) do
        t[k] = t[k] or v
      end

      local mt = getmetatable(e)
      return mt and mt.__index and f(mt.__index, t) or t
    end

    return f(self.env, {})
  elseif varname == '#' then
    return #self.env
  end
  return self.env[varname]
end

--- set an environment variable
function process_metatable:setenv(varname, value)
  checkArg(1, varname, "string", "number", "nil")
  checkArg(2, value, "string", "number", "nil")

  if value ~= nil then
    value = tostring(value)
  end

  self.env[varname] = value
  return value
end

return process
