-- sys/devfs.lua - /dev filesystem implementation
-- vim: set tabstop=2 shiftwidth=2 expandtab :
--

local devfs = {
  data = {} -- tree of files and directories
}

local handle_list = {} -- list of file handles
local handle_next = 1 -- id of next file handle to return
local function get_handle()
  local n = handle_next
  handle_next = (handle_list[n] and handle_list[n].next) or (#handle_list + 2)
  handle_list[n] = { id = n }
  return handle_list[n]
end

local function return_handle(handle)
  local id = handle.id
  handle_list[id] = { next = handle_next }
  handle_next = id
end

-- filesystem implementation
local function read_only()
  return nil, "filesystem read-only"
end
devfs.proxy = {
  type = "filesystem",
  address = "devfs0000",
  spaceUsed = function() return 0 end,
  spaceTotal = function() return 0 end,
  makeDirectory = read_only,
  isReadOnly = function() return true end,
  rename = read_only,
  remove = read_only,
  setLabel = read_only,
  getLabel = function() return "devfs" end,
}

local function get_file(path)
  checkArg(1, path, "string")
  local seg, err = vfs.segments_append_path(nil, path)
  if not seg then
    return nil, err
  end
  local file = devfs.data
  for _, d in pairs(seg) do
    file = file[d]
    if not file then
      return nil, "file does not exist"
    end
  end
  return file
end

function devfs.proxy.size(path)
  local file, err = get_file(path)
  if not file then
    return nil, err
  end
  return file.size and file.size() or 0
end

function devfs.proxy.exists(path)
  local file, err = get_file(path)
  return file and true or false, err
end

function devfs.proxy.open(path)
  local file, err = get_file(path)
  if not file then
    return nil, err
  end

  local handle = get_handle()
  handle.file = file
  if handle.file.open then
    handle.file.open(handle)
  end
  return handle.id
end

function devfs.proxy.read(h, ...)
  local handle = handle_list[h]
  return handle.file.read(handle, ...)
end

function devfs.proxy.close(h)
  local handle = handle_list[h]
  if handle.file.close then
    handle.file.close(handle)
  end

  return_handle(handle)
end

function devfs.proxy.write(h, ...)
  local handle = handle_list[h]
  return handle.file.write(handle, ...)
end

function devfs.proxy.seek(h, ...)
  local handle = handle_list[h]
  return handle.file.seek(handle, ...)
end

function devfs.proxy.istty(h, ...)
  local handle = handle_list[h]
  if handle.file.istty then
    return handle.file.istty(h, ...)
  else
    return false
  end
end

function devfs.proxy.get_wait(h)
  local handle = handle_list[h]
  return handle.file.__wait
end

function devfs.proxy.isDirectory(path)
  local dir, err = get_file(path)
  if not dir then
    return nil, err
  end

  return not dir.__type
end

function devfs.proxy.list(path)
  local dir, err = get_file(path)

  if not dir then
    return nil, err
  end

  if dir.__type then
    return nil, "file is not a directory"
  end

  if dir.list then
    return dir:list()
  else
    local list = {}
    for f, node in pairs(dir) do
      list[#list + 1] = f .. (node.__type and "" or "/")
    end
    return list
  end
end

devfs.data.null = {
  __type = "f",
  write = function() end
}

devfs.data.kmesg = {
  __type = "f",
  write = function(_, data)
    klog(data)
  end
}

devfs.data.zero = {
  __type = "f",
  read = function(_, c)
    c = c or 1
    return ("\0"):rep(c > (2^16) and (2^6) or c)
  end
}
devfs.data.random = {
  __type = "f",
  read = function(_, c)
    c = c or 1
    local s = ""
    for _ = 1, c do
      s = s .. string.char(math.random(0, 255))
    end
    return s
  end
}

local function simple_read(s)
  return {
    __type = "f",
    read = function(h)
      if not h.simple_read_complete then
        h.simple_read_complete = true
        return s
      end
    end,
  }
end

local components = {}
for addr, t in component.list() do
  components[#components + 1] = addr
  components[addr] = {
    ["type"] = simple_read(assert(component.type(addr))),
    ["address"] = simple_read(addr),
    ["slot"] = simple_read(assert(component.slot(addr))),
  }
end

devfs.data.components = {
  ["by-uuid"] = setmetatable({
    list = function()
      return components
    end,
  }, {
    __index = function(_, k)
      if components[k] then
        return components[k]
      end
    end,
  }),
  ["by-type"] = {

  },
}

return devfs
