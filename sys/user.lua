-- sys/user.lua - userspace sandbox implementation
-- vim: set tabstop=2 shiftwidth=2 expandtab :
--

local user = {}

user.baseenv = {
  assert = assert,
  error = error,
  getmetatable = sandbox.getmetatable,
  ipairs = ipairs,
  load = nil,
  next = next,
  pairs = pairs,
  pcall = pcall,
  print = nil,
  rawequal = rawequal,
  rawget = rawget,
  rawlen = rawlen,
  select = select,
  setmetatable = setmetatable,
  tonumber = tonumber,
  tostring = tostring,
  type = sandbox.type,
  _VERSION = _VERSION,
  xpcall = xpcall,
  checkArg = sandbox.checkArg,
}

user.preload = {}

user.preload.string = {
  byte = string.byte,
  char = string.char,
  dump = string.dump,
  find = string.find,
  format = string.format,
  gmatch = string.gmatch,
  gsub = string.gsub,
  len = string.len,
  lower = string.lower,
  match = string.match,
  rep = string.rep,
  reverse = string.reverse,
  sub = string.sub,
  upper = string.upper,
  pack = string.pack,
  unpack = string.unpack,
  packsize = string.packsize
}

user.preload.table = {
  concat = table.concat,
  insert = table.insert,
  pack = table.pack,
  remove = table.remove,
  sort = table.sort,
  unpack = table.unpack,
  move = table.move
}

user.preload.math = {
  abs = math.abs,
  acos = math.acos,
  asin = math.asin,
  atan = math.atan,
  atan2 = math.atan2 or math.atan,
  ceil = math.ceil,
  cos = math.cos,
  cosh = math.cosh,
  deg = math.deg,
  exp = math.exp,
  floor = math.floor,
  fmod = math.fmod,
  frexp = math.frexp,
  huge = math.huge,
  ldexp = math.ldexp,
  log = math.log,
  max = math.max,
  min = math.min,
  modf = math.modf,
  pi = math.pi,
  pow = math.pow,
  rad = math.rad,
  random = math.random,
  randomseed = math.randomseed,
  sin = math.sin,
  sinh = math.sinh,
  sqrt = math.sqrt,
  tan = math.tan,
  tanh = math.tanh,
  maxinteger = math.maxinteger,
  mininteger = math.mininteger,
  tointeger = math.tointeger,
  type = math.type,
  ult = math.ult
}

user.preload.debug = {
  -- these have already been sandboxed by oc
  getinfo = debug.getinfo,
  getlocal = debug.getlocal,
  getupvalue = debug.getupvalue,
  traceback = debug.traceback,
}

user.preload.unicode = {
  len = unicode.len,
  lower = unicode.lower,
  wtrunc = unicode.wtrunc,
  char = unicode.char,
  wlen = unicode.wlen,
  charWidth = unicode.charWidth,
  sub = unicode.sub,
  reverse = unicode.reverse,
  isWide = unicode.isWide,
  upper = unicode.upper,
}

-- bubble any syscalls
user.preload.coroutine = {
  create = coroutine.create,
  isyieldable = coroutine.isyieldable,
  resume = function(co, ...)
    -- bubble syscalls wrapper
    checkArg(1, co, "thread")
    local args = table.pack(...)
    while true do
      local result = table.pack(coroutine.resume(co, table.unpack(args, 1, args.n)))

      if result[1] then
        local state = result[1]
        if coroutine.status(co) == "dead" then
          -- return
          return true, table.unpack(result, 2, result.n)
        elseif state ~= nil then
          -- syscall
          args = table.pack(coroutine.yield(table.unpack(result, 2, result.n)))
        else
          -- coroutine.yield
          return true, table.unpack(result, 3, result.n)
        end
      else
        return false, result[2]
      end
    end
  end,
  running = coroutine.running,
  status = coroutine.status,
  wrap = function(f)
    -- bubble syscalls wrapper
    checkArg(1, f, "function")
    local co = assert(coroutine.create(f))
    return function(...)
      local result = table.pack(user.preload.coroutine.resume(co, ...))
      if result[1] then
        return table.unpack(result, 2)
      else
        error(result[2], 1)
      end
    end
  end,
  yield = function(...)
    -- bubble syscalls wrapper
    return coroutine.yield(nil, ...)
  end
}

-- Setup a new sandbox
-- Every sandbox shares the preloaded modules (user.preload)
-- Every sandbox has its own copy of every module that is otherwise loaded
function user.new()
  local env = {}

  for k, v in pairs(user.baseenv) do
    -- assert(type(v) ~= "table")
    env[k] = v
  end

  env.load = function(ld, src, mode, _env)
    return load(ld, src, mode, _env or env)
  end

  -- list of preloaded userspace libraries
  local preload = setmetatable({}, { __index = user.preload })

  preload.os = {
    clock = function()
      local t2 = os.clock()
      local dt = t2 - process.cproc.clock_last
      return process.cproc.clock + dt
    end,
    date = os.date,
    difftime = os.difftime,
    execute = function(cmd)
      -- FIXME: this should actually invoke the shell
      checkArg(1, cmd, "string")
      local parts = {}
      for part in cmd:gmatch("[^%s]+") do
        table.insert(parts, part)
      end

      return preload.os.spawnp(parts[1],
        process.cproc.io_input,
        process.cproc.io_output,
        process.cproc.io_error,
        table.unpack(parts))
    end,
    exit = process.exit,
    getenv = function(...)
      return process.cproc:getenv(...)
    end,
    setenv = function(...)
      return process.cproc:setenv(...)
    end,
    remove = vfs.remove,
    rename = nil, -- TODO: implement
    setlocale = nil, -- ya know, this might be implemented one day
    time = os.time,
    sleep = process.sleep,
    tmpname = nil, -- TODO: implement
    spawnp = function(prog, stdin, stdout, stderr, ...)
      checkArg(1, prog, "string")
      sandbox.checkArg(2, stdin, "nil", "file")
      sandbox.checkArg(3, stdout, "nil", "file")
      sandbox.checkArg(4, stderr, "nil", "file")

      local f, err = env.loadfile(prog)
      if not f then
        return nil, tostring(err) .. ": " .. tostring(prog)
      end

      local proc
      proc, err = process.new(f, prog, false, process.cproc, prog, ...)
      if not proc then
        return nil, err
      end

      if stdin and sandbox.handle.is_open(stdin) then
        local new_handle
        new_handle, err = sandbox.handle.clone(stdin, proc)
        assert(new_handle, err)
        proc.io_input = new_handle
      else
        proc.io_input = nil
      end

      if stdout and sandbox.handle.is_open(stdout) then
        proc.io_output = assert(sandbox.handle.clone(stdout, proc))
      else
        proc.io_output = nil
      end

      if stderr and sandbox.handle.is_open(stderr) then
        proc.io_error = assert(sandbox.handle.clone(stderr, proc))
      else
        proc.io_error = nil
      end

      return sandbox.handle.new(proc:open(), process.cproc)
    end,
    wait = function(timeout, ...)
      checkArg(1, timeout, "number")

      -- Wait for a number of objects
      local handles = table.pack(...)
      local objs = {}
      for i = 1, handles.n do
        local obj, _ = sandbox.handle.unwrap(handles[i])

        if not obj then
          return error(string.format("bad argument #%d (handle expected, got %s)", i, type(handles[i])))
        end

        if not obj.__wait then
          return error(string.format("bad argument #%d (handle can not wait)", i))
        end

        if obj.remaining then
          local remaining = obj:remaining()
          if remaining and remaining > 0 then
            return handles[i]
          end
        end

        objs[i] = obj.__wait
      end

      local obj, err = process.wait(timeout, table.unpack(objs))
      if not obj then
        return nil, err
      end

      for i = 1, handles.n do
        if objs[i] == obj then
          return handles[i]
        end
      end

      return nil, "unknown"
    end,
  }

  preload.buffer = {
    new = _G.buffer.new,
    pipe = function()
      local a, b = _G.buffer.pipe()
      local a_handle, err = sandbox.handle.new(a, process.cproc)
      if not a_handle then
        b:close()
        return nil, err
      end
      local b_handle
      b_handle, err = sandbox.handle.new(b, process.cproc)
      if not b_handle then
        a_handle:close()
        b:close()
        return nil, err
      end

      return a_handle, b_handle
    end
  }

  preload.io = {
    close = function(file)
      return (file or preload.io.output()):close()
    end,
    flush = function()
      return preload.io.output():flush()
    end,
    pipe = _G.buffer.pipe,
    input = function(file)
      if file then
        if type(file) == "string" then
          assert(0) -- TODO: implement
        elseif not sandbox.type(file) == "file" then
          assert(0) -- TODO: implement
        end

        process.cproc.io_input = file
      end

      return process.cproc.io_input
    end,
    lines = function(filename, ...)
      if filename then
        local file, err = preload.io.open(filename)
        if not file then
          error(err, 2)
        end
        local args = table.pack(...)
        return function()
          local result = table.pack(file:read(table.unpack(args, 1, args.n)))
          if not result[1] then
            if result[2] then
              error(result[2], 2)
            else
              -- end of file
              file:close()
              return nil
            end
          end
          return table.unpack(result, 1, result.n)
        end
      else
        return preload.io.input():lines()
      end
    end,
    open = function(path, mode)
      checkArg(1, path, "string")
      checkArg(2, path, "string", "nil")

      mode = mode or "r"

      if path == "-" then
        if mode:sub(1, 1) == "r" then
          return sandbox.handle.clone(process.cproc.io_input, process.cproc)
        else
          return sandbox.handle.clone(process.cproc.io_output, process.cproc)
        end
      end

      local file, err = vfs.open(path, mode)
      if not file then
        return nil, err
      end

      local buf = buffer.new(mode, file)

      return sandbox.handle.new(buf, process.cproc)
    end,
    output = function(file, mode)
      local _ = mode -- TODO: implement
      if file then
        if type(file) == "string" then
          assert(0) -- TODO: implement
        elseif not sandbox.type(file) == "file" then
          error("bad argument #1 (string or file expected, got " .. type(file) .. ")")
        end

        process.cproc.io_output = file
      end

      return process.cproc.io_output
    end,
    stderr = function()
      return process.cproc.io_error
    end,
    popen = function(cmd, mode)
      checkArg(1, cmd, "string")
      checkArg(2, mode, "string")
      -- FIXME: this should actually invoke the shell

      local parts = {}
      for part in cmd:gmatch("[^%s]+") do
        table.insert(parts, part)
      end

      local prog, err = env.loadfile(parts[1])
      if not prog then
        error(err)
      end

      local proc
      proc, err = process.new(
        prog, parts[1], false, process.cproc, table.unpack(parts)
      )
      assert(proc, err)

      if mode == "w" then
        local newin, sink = buffer.pipe()
        proc.io_input = sandbox.handle.new(newin, proc)
        proc.io_output = process.cproc.io_output
        proc.io_error = process.cproc.io_error
        return sandbox.handle.new(sink, process.cproc)
      elseif mode == "r" or mode == nil then
        local out, newout = buffer.pipe()

        proc.io_input = process.cproc.io_input
        proc.io_output = sandbox.handle.new(newout, proc)
        proc.io_error = process.cproc.io_error

        return sandbox.handle.new(out, process.cproc)
      elseif mode == "rw" or mode == "wr" then
        local newin, sink = buffer.pipe()
        local out, newout = buffer.pipe()

        proc.io_input = sandbox.handle.new(newin, proc)
        proc.io_output = sandbox.handle.new(newout, proc)
        proc.io_error = process.cproc.io_error

        return sandbox.handle.new(sink, process.cproc), sandbox.handle.new(out, process.cproc)
      elseif mode == "" then
        proc.io_input = process.cproc.io_input
        proc.io_output = process.cproc.io_output
        proc.io_error = process.cproc.io_error

        return proc.pid
      end

      return nil, "unallowed mode"
    end,
    read = function(...)
      return preload.io.input():read(...)
    end,
    type = function(file) -- this function is mostly useless
      local t = sandbox.type(file)
      if t ~= "file" then
        return nil
      end

      if sandbox.handle.is_open(file) then
        return "file"
      else
        return "closed"
      end
    end,
    write = function(...)
      return preload.io.output():write(...)
    end,
  }

  preload.libcore = {
    filesystem = {
      segments_append_path = vfs.segments_append_path,
      segments_resolve = vfs.segments_resolve,
      path_to_segments = vfs.path_to_segments,
      canonical = vfs.canonical,
      concat = vfs.concat,
      realPath = vfs.realPath,
      path = vfs.path,
      name = vfs.name,
      exists = vfs.exists,
      isDirectory = vfs.isDirectory,
      list = vfs.list,
      makeDirectory = vfs.makeDirectory,
      lastModified = vfs.lastModified,
      mounts = vfs.mounts,
      isLink = vfs.isLink,
      link = vfs.link,
      size = vfs.size,
    },
    system = {
      list_fs = function()
        local l = {}
        for uuid, t in component.list("filesystem") do
          l[#l + 1] = uuid
        end

        return next, l, nil
      end,
      mount = function(uuid, path)
        checkArg(1, uuid, "string")
        checkArg(2, path, "string")
        local t, err = component.type(uuid)
        if not t then return nil, err end

        if t ~= "filesystem" then
          return nil, "component is not a filesystem"
        end

        local fs_proxy
        fs_proxy, err = component.proxy(uuid)
        if not fs_proxy then return nil, err end

        return vfs.mount(fs_proxy, path)
      end,
      unmount = function(path_or_uuid)
        checkArg(1, path_or_uuid, "string")
        return vfs.umount(path_or_uuid)
      end,
    }
  }

  preload.net = {
    list_modems = net.list_modems,
    socket = {
      open = function(addr)
        local soc, err = net.socket.new(addr)
        if not soc then
          return nil, err
        end

        return sandbox.handle.new(soc, process.cproc)
      end,
    }
  }

  preload.computer = {
    address = computer.address, -- FIXME: might not be necessary for userspace to know this
    tmpAddress = computer.tmpAddress, -- same for this
    freeMemory = computer.freeMemory,
    totalMemory = computer.totalMemory,
    energy = computer.energy,
    maxEnergy = computer.maxEnergy,
    uptime = computer.uptime,
    beep = computer.beep,
    shutdown = function()
      -- FIXME: this should be implemented by userspace
      return nil, "not implemented"
    end,
  }

  preload.package = {
    config = "/\n;\n;?\n!\n-",
  }

  -- remove unused loaded packages to reduce memory pressure
  local loaded = setmetatable({}, { __mode = "kv" })
  preload.package.loaded = loaded

  preload.package.searchpath = function(name, path, sep, rep)
    checkArg(1, name, "string")
    checkArg(2, path, "string")
    sep = sep or '.'
    rep = rep or '/'
    sep = '%' .. sep
    checkArg(3, sep, "string")
    checkArg(4, rep, "string")

    name = string.gsub(name, sep, rep)
    local errorFiles = {}
    for subpath in string.gmatch(path, "([^;]+)") do
      subpath = string.gsub(subpath, "?", name)
      if string.sub(subpath, 1, 1) ~= "/" and process.cproc then
        -- only run this if we were called from a user process
        subpath = vfs.concat(process.cproc:getenv("PWD") or "/", subpath)
      end
      if vfs.exists(subpath) then
        local f = vfs.open(subpath, "r")
        if f then
          f:close()
          return subpath
        end
      end
      table.insert(errorFiles, "\tno file '" .. subpath .."'")
    end
    return nil, table.concat(errorFiles, "\n")
  end

  local function searcher_preload(module)
    return preload[module]
  end

  local function searcher_path(module)
    local searchpath =
      process.cproc:getenv("LIBPATH") or
      "/lib/?.lua;/usr/lib/?.lua;./?.lua;/lib/?/init.lua;/usr/lib/?/init.lua;./?/init.lua"
    local path, err = preload.package.searchpath(module, searchpath)
    if path then
      local loader
      -- we don't want libraries to modify _G directly
      loader, err = env.loadfile(path, "bt", setmetatable({}, { __index = env }))
      if loader then
        local success
        success, err = pcall(loader)
        if success then
          return err
        end
      end
    end
    return nil, err
  end

  local loading = {}

  preload.package.searchers = {}
  preload.package.searchers[#preload.package.searchers + 1] = searcher_preload
  preload.package.searchers[#preload.package.searchers + 1] = searcher_path

  env.require = function(name)
    checkArg(1, name, "string")

    if loaded[name] then
      return loaded[name]
    else
      if loading[name] then
        -- FIXME: we might be able to use some metatable magic to "defer" the actual load
        error("already loading " .. name)
      else
        loading[name] = true

        local reason
        for _, searcher in ipairs(preload.package.searchers) do
          local success, mod, err = pcall(searcher, name)
          if success and mod then
            loading[name] = nil
            loaded[name] = mod
            return mod
          elseif (not success) and mod then
            reason = mod
          elseif err then
            reason = err
          end
        end

        loading[name] = nil
        error(string.format("Could not load module '%s': %s", name, reason or "module returned nil"))
      end
    end
  end

  -- "load" all necessary libraries
  env.string = preload.string
  env.table = preload.table
  env.math = preload.math
  env.debug = preload.debug
  env.unicode = preload.unicode
  env.package = preload.package
  env.os = preload.os
  env.io = preload.io
  env.coroutine = preload.coroutine

  env.loadfile = function(filename, mode, new_env)
    checkArg(1, filename, "string")
    checkArg(2, mode, "string", "nil")
    checkArg(3, new_env, "table", "nil")

    local f, err = vfs.open(filename, "r")
    if not f then
      return nil, err
    end

    local buf = {}
    while true do
      local v
      v, err = f:read(math.huge)

      if not v then
        f:close()
        if err then
          return nil, err
        end
        break
      end
      buf[#buf + 1] = v
    end

    local s = table.concat(buf)
    return load(s, "=" .. filename, mode or "bt", new_env or env)
  end

  -- XXX: use of this function is discouraged, just use `assert(loadfile(filename))(...)` instead
  env.dofile = function(filename)
    local f, err = env.loadfile(filename)
    if not f then
      return error(err .. ':' .. filename, 0)
    end
    return f()
  end

  env.print = function(...)
    local args = table.pack(...)
    local stdout = preload.io.output()
    local pre = ""
    for i = 1, args.n do
      stdout:write(pre)
      stdout:write(tostring(args[i]))
      pre = "\t"
    end
    stdout:write("\n")
    stdout:flush()
  end

  env._G = env

  return env
end

return user
