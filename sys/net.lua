-- sys/net.lua - networking
-- vim: set tabstop=2 shiftwidth=2 expandtab :
--

local component = component

local net = {}

function net.list_modems()
  return component.list("modem")
end

net.socket = {}

local socket_mt = {
  __index = net.socket,
}

function net.socket.new(modem_addr)
  checkArg(1, modem_addr, "string")

  if component.type(modem_addr) ~= "modem" then
    return nil, "no such modem"
  end

  local modem, err = component.proxy(modem_addr)
  if not modem then
    return nil, err
  end

  return setmetatable({
    modem = modem,
    __type = "socket",
    __ref = 1,
  }, socket_mt)
end

function net.socket:open()
  if not self.__ref then
    return nil, "closed"
  end

  self.__ref = self.__ref + 1
  return self
end

function net.socket:close()
  assert(self.__ref, "double close")

  self.__ref = self.__ref - 1
  if self.__ref == 0 then
    self.__ref = nil
    return self:cleanup()
  end
end

function socket_mt:__tostring()
  return string.format("<net socket { __wait = %s }>", tostring(self.__wait))
end

function net.socket:send(daddr, port, ...)
  checkArg(1, daddr, "string", "nil")
  checkArg(2, port, "number")
  if not daddr then
    return self.modem.broadcast(port, ...)
  end

  return pcall(self.modem.send(daddr, port, ...))
end

kevent.listen("signal", function(_, signal_name, daddr, saddr, dport, ...)
  if signal_name == "modem_message" then
    -- klog("net: msg from %s to %s:%s", tostring(saddr), tostring(daddr), tostring(dport))

    if daddr == saddr then
      -- Drop packets that originated from ourselfs
      return
    end

    for port, socket in pairs(net.sockets) do
      -- klog("consider socket %s (port %d ?? %d)", tostring(socket), dport, port)
      if port == dport then
        local event = { daddr, saddr, dport, ... }

        table.insert(socket.incoming_buffer, event)

        process.notify(socket.incoming_buffer)

        break
      end
    end
  end
end)

function net.socket:remaining()
  return self.incoming_buffer and #self.incoming_buffer
end

function net.socket:receive(timeout)
  checkArg(1, timeout, "number", "nil")
  if not self.open_port then
    return nil, "socket not bound"
  end

  local msg = table.remove(self.incoming_buffer)
  if not msg then
    while true do
      local obj, err = process.wait(timeout, self.incoming_buffer)
      if not obj then
        return nil, err
      end

      if obj == self.incoming_buffer then
        msg = table.remove(self.incoming_buffer)
        if msg then
          break
        end
      end
    end
  end

  return table.unpack(msg)
end

net.sockets = {}

function net.socket:bind(port)
  checkArg(1, port, "number")
  if self.open_port then
    return nil, "socket already bound"
  end

  local success, err = self.modem.open(port)
  if not success then
    return nil, err
  else
    self.open_port = port
    self.incoming_buffer = {}
    self.__wait = self.incoming_buffer
    net.sockets[port] = self
    return success
  end
end

function net.socket:cleanup()
  local open_port = self.open_port
  if open_port then
    self.open_port = nil
    net.sockets[open_port] = nil
    return assert(self.modem.close(open_port))
  end
end

return net
