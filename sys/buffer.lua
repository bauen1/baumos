-- sys/buffer.lua - file buffer / pipe implementation
-- vim: set tabstop=2 shiftwidth=2 expandtab :

---
-- @classmod buffer

local buffer = {}

local metatable = {
  __index = buffer,
}

--- create a new buffer
-- @tparam string mode
-- @tparam stream stream
-- @treturn buffer self
function buffer.new(mode, stream)
  checkArg(1, mode, "string", "nil")

  local obj = {
    mode = {},
    stream = stream,
    __wait = stream.__wait or stream.get_wait and stream:get_wait(),
    bufferRead = "",
    bufferWrite = "",
    bufferSize = math.floor(math.max(512, math.min(8 * 1024, computer.freeMemory() / 8))),
    bufferMode = "full",
    readTimeout = math.huge,
    __type = "file",
    __ref = 1,
  }

  mode = mode or "r"
  for i = 1, unicode.len(mode) do
    obj.mode[unicode.sub(mode, i, i)] = true
  end
  return setmetatable(obj, metatable)
end

--- create a new pipe
-- @treturn buffer pipe input
-- @treturn buffer pipe output
function buffer.pipe()
  local inStream = {}
  local outStream = {}

  local isOpen = true

  local buf = ""

  function inStream:close()
    local _ = self
    buf = nil
    return true
  end

  function outStream:close()
    local _ = self
    isOpen = false
    return true
  end

  function outStream:write(str)
    local _ = self
    if not buf then
      return nil, "pipe closed"
    end

    buf = buf .. str

    process.notify(inStream)
    return self
  end

  function inStream:read(n, timeout)
    local _ = self
    checkArg(1, n, "number", "nil")
    checkArg(2, timeout, "number", "nil")
    if not buf then
      return nil, "pipe closed"
    end

    if #buf == 0 and n ~= 0 then
      while isOpen do
        local obj, err = process.wait(timeout or math.huge, inStream)
        if not obj then
          return nil, err
        end

        if obj == inStream then
          break
        end
      end

      if #buf == 0 and not isOpen then
        buf = nil
      end
    end

    local result = buf
    buf = buf and ""
    return result
  end

  local function badFileDescriptor()
    return nil, "bad file descriptor"
  end

  inStream.seek = badFileDescriptor
  inStream.write = badFileDescriptor
  inStream.remaining = function()
    return buf and #buf or -1
  end
  outStream.read = badFileDescriptor
  outStream.seek = badFileDescriptor

  inStream.__wait = inStream

  local _in = buffer.new("r", inStream)
  local out = buffer.new("w", outStream)

  out:setvbuf("no")

  return _in, out
end

---
-- @treturn string
function metatable:__tostring()
  return string.format("<buffer { stream = %s __wait = %s #bufferRead = %d}>", tostring(self.stream), tostring(self.__wait), #self.bufferRead)
end

---
-- @treturn[1] integer remaining bytes to be read
-- @treturn[2] nil
-- @treturn[2] string error message
function buffer:remaining()
  if self.stream.remaining then
    return #self.bufferRead + self.stream:remaining()
  else
    return nil, "operation not supported"
  end
end

---
-- @treturn buffer self
function buffer:open()
  if not self.__ref then
    return nil, "closed"
  end

  self.__ref = self.__ref + 1
  return self
end

---
function buffer:close()
  assert(self.__ref, "double close")

  if self.mode.w or self.mode.a then
    self:flush()
  end

  self.__ref = self.__ref - 1
  if self.__ref == 0 then
    self.__ref = nil
    local stream = self.stream
    self.stream = nil
    return stream:close()
  end

  return true
end

--- flush everything that is buffered for write
-- @treturn boolean
function buffer:flush()
  local result, reason = self.stream:write(self.bufferWrite)
  if result then
    self.bufferWrite = ""
  else
    return nil, reason or "bad file descriptor"
  end

  return result
end

function buffer:readChunk(deadline, n)
  if computer.uptime() > deadline then
    return nil, "timeout"
  end

  n = n or self.bufferSize

  -- FIXME: pass along timeout if possible
  local result, err = self.stream:read(n)
  if not result then
    return nil, err
  end

  assert(string.len(result) > 0 and result ~= "")
  self.bufferRead = self.bufferRead .. result
  return self
end

---
-- @treturn[1] string
function buffer:read(...)
  local deadline = computer.uptime() + self.readTimeout

  local function readBytesOrChars(n)
    n = math.tointeger(n)
    if not n then
      return nil, "number has no integer presentation"
    end

    -- shortly said: unicode.* breaks for integers at about 2^31
    n = math.min(math.max(n, 0), 2^30)

    local len, sub
    if self.mode.b then
      len = rawlen
      sub = string.sub
    else
      len = unicode.len
      sub = unicode.sub
    end

    if n == 0 then
      local result, err = self:readChunk(deadline, n)
      if not result then
        if err then
          return nil, err
        end
      end

      local buf = self.bufferRead
      self.bufferRead = ""
      return buf
    end

    local buf = ""
    repeat
      if len(self.bufferRead) == 0 then
        local result, err = self:readChunk(deadline)
        if not result then
          if err then
            return nil, err
          else -- end of file
            return #buf > 0 and buf or nil
          end
        end
      end

      local left = n - len(buf)
      buf = buf .. sub(self.bufferRead, 1, left)
      self.bufferRead = sub(self.bufferRead, left + 1)
    until len(buf) >= n

    return buf
  end

  local function readNumber()
    return nil, "todo: implement"
  end

  local function readLine(chop)
    -- FIXME: verify
    local start = 1
    while true do
      local l = self.bufferRead:find("\n", start, true)
      if l then
        local result = self.bufferRead:sub(1, l + (chop and -1 or 0))
        self.bufferRead = self.bufferRead:sub(l + 1)
        return result
      else
        start = #self.bufferRead
        local v, err = self:readChunk(deadline)
        if not v then
          if err then
            return nil, err
          else -- end of file
            local result = #self.bufferRead > 0 and self.bufferRead or nil
            self.bufferRead = ""
            return result
          end
        end
      end
    end
  end

  local function readAll()
    repeat
      local result, err = self:readChunk(deadline)
      if not result and err then
        return nil, err
      end
    until not result -- end of file
    local result = self.bufferRead
    self.bufferRead = ""
    return result
  end

  local function read(n, format)
    if type(format) == "number" then
      return readBytesOrChars(format)
    else
      if type(format) ~= "string" or unicode.sub(format, 1, 1) ~= "*" then
        return nil, "bad argument #" .. n .. " (invalid option)"
      end
      format = unicode.sub(format, 2, 2)
      if format == "n" then
        return readNumber()
      elseif format == "l" then
        return readLine(true)
      elseif format == "L" then
        return readLine(false)
      elseif format == "a" then
        return readAll()
      else
        return nil, "bad argument #" .. n .. " (invalid format)"
      end
    end
  end

  if self.mode.w or self.mode.a then
    self:flush()
  end

  local formats = table.pack(...)

  if formats.n == 0 then
    return readLine(true)
  end

  local results = {}
  for i = 1, formats.n do
    local result, err = read(i, formats[i])
    if result then
      results[i] = result
    elseif err then
      return nil, err
    end
  end

  return table.unpack(results, 1, formats.n)
end

---
-- @tparam string whence defaults to "cur"
-- @tparam integer offset
function buffer:seek(whence, offset)
  whence = tostring(whence or "cur")
  assert(
    whence == "set" or
    whence == "cur" or
    whence == "end",
    "bad argument #1 (set, cur or end expected, got " .. whence .. ")")
  offset = offset or 0
  checkArg(2, offset, "number")
  assert(math.floor(offset) == offset, "bad argument #2 (not an integer)")

  if self.mode.w or self.mode.a then
    self:flush()
  elseif whence == "cur" then
    offset = offset - #self.bufferRead
  end
  local result, err = self.stream:seek(whence, offset)
  if result then
    self.bufferRead = ""
    return result
  else
    return nil, err
  end
end

--- set the buffering mode and size
-- @tparam[opt] string mode allowed values: "no", "full", "line"
-- @tparam[opt] integer size
-- @treturn string new buffer mode
-- @treturn integer new buffer size
function buffer:setvbuf(mode, size)
  mode = mode or self.bufferMode
  checkArg(1, mode, "string")
  size = size or self.bufferSize
  checkArg(2, size, "number")

  assert(
    mode == "no" or
    mode == "full" or
    mode == "line",
    "bad argument #1 (no, full or line expected, got " .. tostring(mode) .. ")")
  assert(
    mode == "no" or type(size) == "number", "bad argument #2 (number expected, got " .. type(size) .. ")")
  self.bufferMode = mode
  self.bufferSize = size

  return self.bufferMode, self.bufferSize
end

---
-- @treturn number
function buffer:getTimeout()
  return self.readTimeout
end

---
-- @tparam number value
function buffer:setTimeout(value)
  self.readTimeout = tonumber(value)
end

---
-- @tparam string ...
function buffer:write(...)
  if not self.__ref then
    return nil, "bad file descriptor"
  end

  local args = table.pack(...)
  for i = 1, args.n do
    if type(args[i]) == "number" then
      args[i] = tostring(args[i])
    end
    checkArg(i, args[i], "string")
  end

  for i = 1, args.n do
    local arg = args[i]
    local result, err
    if self.bufferMode == "full" then
      if self.bufferSize - #self.bufferWrite < #arg then
        result, err = self:flush()
        if not result then
          return nil, err
        end
      end
      if #arg > self.bufferSize then
        result, err = self.stream:write(arg)
      else
        self.bufferWrite = self.bufferWrite .. arg
        result = self
      end
    elseif self.bufferMode == "line" then
      local l
      repeat
        local idx = arg:find("\n", (l or 0) + 1, true)
        if idx then
          l = idx
        end
      until not idx
      if l or #arg > self.bufferSize then
        result, err = self:flush()
        if not result then
          return nil, err
        end
      end
      if l then
        result, err = self.stream:write(arg:sub(1, l))
        if not result then
          return nil, err
        end
        arg = arg:sub(l + 1)
      end
      if #arg > self.bufferSize then
        result, err = self.stream:write(arg)
      else
        self.bufferWrite = self.bufferWrite .. arg
        result = self
      end
    else -- no buffering
      result, err = self.stream:write(arg)
    end

    if not result then
        return nil, err
    end
  end

  return true
end

return buffer
