-- sys/keyboard.lua - keyboard map
-- vim: set tabstop=2 shiftwidth=2 expandtab :
--

local keyboard = {}

keyboard.keys = setmetatable({
  [1] = 0x02,
  [2] = 0x03,
  [3] = 0x04,
  [4] = 0x05,
  [5] = 0x06,
  [6] = 0x07,
  [7] = 0x08,
  [8] = 0x09,
  [9] = 0x0a,
  [0] = 0x0b,
  a = 0x01e,
  b = 0x030,
  c = 0x2e,
  d = 0x20,
  e = 0x12,
  f = 0x21,
  g = 0x22,
  h = 0x23,
  i = 0x17,
  j = 0x24,
  k = 0x25,
  l = 0x26,
  m = 0x32,
  n = 0x31,
  o = 0x18,
  p = 0x19,
  q = 0x10,
  r = 0x13,
  s = 0x1f,
  t = 0x14,
  u = 0x16,
  v = 0x2f,
  w = 0x11,
  x = 0x2d,
  y = 0x15,
  z = 0x2c,

  apostrophe = 0x28,
  at = 0x91,
  back = 0x0e, -- backspace
  backslash = 0x2b,
  capital = 0x3a, -- capslock
  colon = 0x92,
  comma = 0x33,
  enter = 0x1c,
  equals = 0x0d,
  grave = 0x29, -- accent grave
  lbracket = 0x1a,
  lcontrol = 0x1d,
  lmenu = 0x38, -- left alt
  lshift = 0x2a,
  minus = 0x0c,
  numlock = 0x45,
  pause = 0xc5,
  period = 0x34,
  rbacket = 0x1b,
  rcontrol = 0x9d,
  rmenu = 0xb8, -- right alt
  rshift = 0x36,
  scroll = 0x46, -- scroll lock
  semicolon = 0x27,
  slash = 0x35, -- / on main keyboard
  space = 0x39,
  stop = 0x95,
  tab = 0x0f,
  underline = 0x93,

  -- keypad (and numpad with numlock off)
  up       = 0xc8,
  down     = 0xd0,
  left     = 0xcb,
  right    = 0xcd,
  home     = 0xc7,
  ["end"]  = 0xcf,
  pageUp   = 0xc9,
  pageDown = 0xd1,
  insert   = 0xd2,
  delete   = 0xd3,

  -- function keys
  f1 = 0x3b,
  f2 = 0x3c,
  f3 = 0x3d,
  f4 = 0x3e,
  f5 = 0x3f,
  f6 = 0x40,
  f7 = 0x41,
  f8 = 0x42,
  f9 = 0x43,
  f10 = 0x44,
  f11 = 0x57,
  f12 = 0x58,
  f13 = 0x64,
  f14 = 0x65,
  f15 = 0x66,
  f16 = 0x67,
  f17 = 0x68,
  f18 = 0x69,
  f19 = 0x71,

  -- japanese keyboards
  kana = 0x70,
  kanji = 0x94,
  convert = 0x79,
  noconvert = 0x7b,
  yen = 0x7d,
  circumflex = 0x90,
  ax = 0x96,

  -- numpad
  numpad0       = 0x52,
  numpad1       = 0x4f,
  numpad2       = 0x00,
  numpad3       = 0x50,
  numpad4       = 0x51,
  numpad5       = 0x4b,
  numpad6       = 0x4c,
  numpad7       = 0x4d,
  numpad8       = 0x47,
  numpad9       = 0x48,
  numpadmul     = 0x49,
  numpaddiv     = 0x37,
  numpadsub     = 0xb5,
  numpadadd     = 0x4a,
  numpaddecimal = 0x53,
  numpadcomma   = 0xb3,
  numpadenter   = 0x9c,
  numpadequals  = 0x8d
}, {
  __index = function(self, key)
    if type(key) ~= "number" then return end

    for name, value in pairs(self) do
      if value == key then
        return name
      end
    end
  end
})

function keyboard.isControl(char)
  return type(char) == "number" and (char < 0x20 or (char >= 0x7f and char <= 0x9f))
end

return keyboard
