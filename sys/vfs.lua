-- sys/vfs.lua - virtual filesystem implementation
-- vim: set tabstop=2 shiftwidth=2 expandtab :

--- Virtual Filesystem Implementation
-- @module vfs

--- Virtual Filesystem Node
-- @type node

---
-- @field[type=string] name

---
-- @field[type=filesystem,opt] fs

---
-- @field[type=table] children

---
-- @field[type=table] links

---
-- @section end

local vfs = {}

local mtab = {
  -- path element
  name = "",

  -- filesystem component proxy
  fs = nil,

  -- filesystem children nodes
  children = {
    -- [<value.name>] = node:<value>
    -- assert(children.X.name == X)
    -- can be recursive
  },

  -- symlinks
  links = {
    -- [string:<name>] = string:<dest>
  }
}

--- Append a path onto a table of segments
-- @tparam table segments
-- @tparam string path
-- @treturn table
function vfs.segments_append_path(segments, path)
  checkArg(1, segments, "table", "nil")
  checkArg(2, path, "string")

  segments = segments or {}
  -- some people might prefer windows style paths (for whatever reason)
  path = path:gsub("\\", "/")

  for part in path:gmatch("[^/]+") do
    segments[#segments + 1] = part
  end

  return segments
end

--- try to resolve a relative path as best as possible
-- @tparam table segments
-- @tparam table partial
-- @treturn table
function vfs.segments_resolve(segments, partial)
  local i = 1
  while true do
    local segment = segments[i]

    if not segment then
      break
    elseif segment == "." then
      table.remove(segments, i)
    elseif segment == ".." then
      local parent = segments[i - 1]

      if parent and parent ~= ".." then
        -- if we can remove this and a parent do so
        table.remove(segments, i)
        table.remove(segments, i - 1)
        i = i - 1
      elseif not partial then
        -- don't leave any .. in the table
        table.remove(segments, i)
      else
        i = i + 1
      end
    else
      i = i + 1
    end
  end

  return segments
end

--- Helper to simplify invoking @{vfs.segments_append_path} and @{vfs.segments_resolve}
-- @tparam string path
-- @tparam boolean allow_relative
-- @tparam boolean resolve_special
-- @treturn[1] table
-- @treturn[2] nil
-- @treturn[2] string error message
function vfs.path_to_segments(path, allow_relative, resolve_special)
  checkArg(1, path, "string")

  local segments, err = {}

  if resolve_special then
    if unicode.sub(path, 1, 1) ~= "/" then
      if unicode.sub(path, 1, 2) == "~/" then
        local home = (process.cproc and process.cproc:getenv("HOME")) or "/"
        path = unicode.sub(path, 3)
        segments, err = vfs.segments_append_path(segments, home)
        if not segments then
          return nil, err
        end
      else
        local pwd = (process.cproc and process.cproc:getenv("PWD")) or "/"
        segments, err = vfs.segments_append_path(segments, pwd)
        if not segments then
          return nil, err
        end
      end
    end
  end

  segments, err = vfs.segments_append_path(segments, path)
  if not segments then
    return nil, err
  end

  return vfs.segments_resolve(segments, allow_relative)
end

---
-- @tparam string path
-- @treturn[1] string
-- @treturn[2] nil
-- @treturn[2] string error message
function vfs.canonical(path)
  checkArg(1, path, "string")

  local segments, err = vfs.path_to_segments(path, false, true)
  if not segments then
    return nil, err
  end

  return (unicode.sub(path, 1, 1) == "/" and "/" or "") .. table.concat(segments, "/")
end

---
-- @tparam string ...
-- @treturn[1] string
-- @treturn[2] nil
-- @treturn[2] string error message
function vfs.concat(...)
  local args = table.pack(...)

  local segments = {}
  local err
  local absolute

  for i, v in ipairs(args) do
    checkArg(i, v, "string")
    segments, err = vfs.segments_append_path(segments, v)
    if not segments then
      return nil, err
    end

    if absolute == nil then
      if unicode.sub(v, 1, 1) == "/" then
        absolute = true
      end
    end
  end

  -- FIXME: maybe false here ?
  segments, err = vfs.segments_resolve(segments, true)
  if not segments then
    return nil, err
  end

  local p = table.concat(segments, "/")

  if absolute then
    return "/" .. p
  else
    return p
  end
end

do
  -- do a bit of unit testing on segments_resolve
  local function test(input, expected, ...)
    local function clone_table(value)
      local new = {}
      for k,v in pairs(value) do
        new[k] = v
      end
      return new
    end
    local output = vfs.segments_resolve(clone_table(input), ...)
    local function equals(a, b)
      if #a ~= #b then return false end
      for k, v in pairs(a) do
        if b[k] ~= v then return false end
      end
      return true
    end
    if not equals(expected, output) then
      local function log(name, v)
        name = tostring(name)
        klog("%s = {", name)
        for k,v2 in pairs(v) do
          klog("[%s] = %s", tostring(k), tostring(v2))
        end
        klog("}")
      end
      log("input", input)
      log("expected", expected)
      log("output", output)
      error("unit test failed!")
    end
  end

  test({ "..", "b" }, { "b" }, false)
  test({ "..", "b", "." }, { "..", "b" }, true)
  test({"abc", "d", "e", "..", ".", "l", ".", "..", "d"}, {"abc", "d", "d"}, true)

  vfs.concat("abc", "..")
  assert(vfs.concat("abc/d/e", ".././e", "./../", "d") == "abc/d/d")
end

-- Helper for findNode: find the last node that has a filesystem
local function index_of_last_fs_node(nodes, i)
  for j = i, 1, -1 do
    if nodes[j].fs then
      return j
    end
  end
end

--- walks the vfs mount tree
-- @function findNode
-- @tparam table parts
-- @tparam boolean create create missing nodes ?
-- @tparam boolean resolve_links follow symlinks on last node
-- @tparam[opt] integer depth depth of recursive function call (can b)
-- @treturn[1] vfs.node the closest (ie. most distance from root) node where fs ~= nil
-- @treturn[1] string the rest of the path from the last node where fs ~= nil
-- @treturn[1,opt] vfs.node the last virtual node
-- @treturn[1,opt] string the rest of the path
-- @treturn[2] nil
-- @treturn[2] string error message
local function findNode(parts, create, resolve_links, depth)
  checkArg(1, parts, "table")
  checkArg(2, create, "boolean")
  checkArg(3, resolve_links, "boolean")
  depth = depth or 0
  if depth > 10 then
    return nil, "link cycle"
  end

  -- list of the node tree we walked
  local nodes = {}

  -- current node
  local node = mtab

  -- i = number of parts "used"
  local i = 1
  while i <= #parts do
    local part = parts[i]
    nodes[i] = node

    if not node.children[part] then
      if node.links[part] then
        assert(0)
        -- symlink
        if not resolve_links and #parts == i then
          -- be we're supposed to return it anyway
          break
        end

        local link = node.links[part]

        -- TODO: untested / broken after this
        if unicode.sub(link, 1, 1) == "/" then -- absolute
          local segments, err = vfs.path_to_segments(link, true, false)
          if not segments then
            return nil, err
          end
          return findNode(segments, create, resolve_links, depth + 1)
        else -- relative
          -- parent seg
          assert(i > 1)

          -- p = parent path
          local p = table.pack(table.unpack(parts, 1, i - 1))
          -- r = rest path
          local r = table.pack(table.unpack(parts, i))
          -- s = new segments
          local new_segments = p
          local err
          new_segments, err = vfs.segments_append_path(new_segments, link)
          if not new_segments then
            return nil, err
          end
          for _, v in pairs(r) do
            table.insert(new_segments, v)
          end
          new_segments, err = vfs.segments_resolve(new_segments)
          if not new_segments then
            return nil, err
          end
          return findNode(new_segments, create, resolve_links, depth + 1)
        end
      else
        -- child does not exist
        if create then
          -- create new node
          node.children[part] = {
            name = part,
            parent = node,
            children = {},
            links = {},
            fs = nil,
          }
        else
          break
        end
      end
    end

    -- normal walk
    node = node.children[part]
    i = i + 1
  end
  -- add the last node, or if we didn't actually walk add the root node to the list
  nodes[i] = node

  -- we reached (maybe a full) end of the tree
  assert(node)
  local vnode = node -- last node we managed to find
  local vrest = #parts >= i and table.concat(parts, "/", i) -- rest after vnode

  local j = index_of_last_fs_node(nodes, #nodes) -- index of fs node
  local fs_node = nodes[j]
  local rest = table.concat(parts, "/", j)

  return fs_node, rest, vnode, vrest
end
vfs.findNode = findNode

--- Debug helper
function vfs.dumpMounts()
  local visited = {}
  local function f(node, depth)
    if depth > 10 then
      return nil
    end
    if not visited[node] then
      f(node, depth + 1)
      visited[node] = true
    end
    local depth_s = string.rep(" ", depth)
    klog("%s%s = {name: '%s', fs: %s}", depth_s, tostring(node), tostring(node.name), tostring(node.fs))
    for _, v in pairs(node.children or {}) do
      f(v, depth + 1)
    end
    for k, v in pairs(node.links or {}) do
      klog("%s%s -> %s", depth_s, tostring(k), tostring(v))
    end
  end
  return f(mtab, 0)
end

-- TODO: rewrite everything below here
-- TODO: remove any "double" calls to segments_*

--- Get the real path of a file (ie. after resovling all symlinks)
-- @tparam string path
-- @treturn[1] string real path after resolving all symlinks
-- @treturn[2] nil
-- @treturn[2] string error message
function vfs.realPath(path)
  checkArg(1, path, "string")

  local segments, err = vfs.path_to_segments(path, true, true)
  if not segments then
    return nil, err
  end

  local node, rest = findNode(segments, false, true)
  if not node then
    return nil, rest
  end

  local parts = { rest or nil }
  repeat
    table.insert(parts, 1, node.name)
    node = node.parent
  until not node

  return table.concat(parts, "/")
end


--- Mount a filesystem
-- This is a bit stricter than the version on OpenOS
-- @tparam filesystem fs
-- @tparam string path
-- @treturn[1] boolean success
-- @treturn[2] nil
-- @treturn[2] string error message
function vfs.mount(fs, path)
  checkArg(1, fs, "table")
  checkArg(2, path, "string")
  assert(fs.type == "filesystem", "not a filesystem")

  klog("vfs.mount(fs=%s, path=%s)", tostring(fs), tostring(path))

  local segments, err = vfs.path_to_segments(path, false, false)
  if not segments then
    return nil, err
  end

  local _, _, vnode, _ = findNode(segments, true, true)
  if vnode.fs then
    return nil, "another filesystem is already mounted here"
  end
  vnode.fs = fs

  return true
end

---
-- FIXME: what is this even used for ?
-- @tparam string path
-- @treturn[1] string
-- @treturn[2] nil
-- @treturn[2] string error message
function vfs.path(path)
  checkArg(1, path, "string")

  local parts, err = vfs.path_to_segments(path, false, false)
  if not parts then
    return nil, err
  end
  local result = table.concat(parts, "/", 1, #parts - 1) .. "/"
  if unicode.sub(path, 1, 1) == "/" and unicode.sub(result, 1, 1) ~= "/" then
    return "/" .. result
  else
    return result
  end
end

--- Returns everything after the last /
-- @tparam string path
-- @treturn[1] string
-- @treturn[2] nil
-- @treturn[2] string error message
function vfs.name(path)
  checkArg(1, path, "string")

  -- FIXME: this could be more efficient
  local parts, err = vfs.path_to_segments(path, false, true)
  if not parts then
    return nil, err
  end

  return parts[#parts]
end

--- Check if a file exists
-- @tparam string path
-- @treturn[1] boolean
-- @treturn[2] nil
-- @treturn[2] string errror message
function vfs.exists(path)
  checkArg(1, path, "string")

  local segments, err = vfs.path_to_segments(path, false, true)
  if not segments then
    return nil, err
  end

  local node, rest, vnode, vrest = findNode(segments, false, false)
  if not vrest or vnode.links[vrest] then
    return true
  elseif node and node.fs then
    return node.fs.exists(rest)
  end

  return false
end

--- Check if a path is a directory
-- @tparam string path
-- @treturn[1] boolean
-- @treturn[2] nil
-- @treturn[2] string error message
function vfs.isDirectory(path)
  local real, reason = vfs.realPath(path)
  if not real then
    return nil, reason
  end

  local segments, err = vfs.path_to_segments(real, false, true)
  if not segments then
    return nil, err
  end

  local node, rest, vnode, vrest = findNode(segments, false, false)

  if not vnode.fs and not vrest then
    return true
  end

  if node.fs then
    if node.fs.isDirectory then
      return not rest or node.fs.isDirectory(rest)
    else
      return not rest
    end
  end

  return false
end

---
-- @tparam string path
-- @treturn[1] iterator Iterator over all files in the directory
-- @treturn[2] nil
-- @treturn[2] string error message
function vfs.list(path)
  local segments, err = vfs.path_to_segments(path, false, true)
  if not segments then
    return nil, err
  end

  local node, rest, vnode, vrest = findNode(segments, false, true)
  local result = {}
  if node then
    if node.fs then
      result = node.fs.list(rest or "")
    end

    if not vrest then
      for k, _ in pairs(vnode.children) do
        table.insert(result, k .. "/")
      end
      for k in pairs(vnode.links) do
        table.insert(result, k)
      end
    end
  end

  local set = {}

  for _, name in ipairs(result or {}) do
    set[vfs.canonical(name)] = name
  end

  return function()
    local key, value = next(set)
    set[key or false] = nil
    return value
  end
end

local mode_check = { r = true, rb = true, w = true, wb = true, a = true, ab = true }

--- Opens a file
-- @tparam string path
-- @tparam[opt] string mode, one of 'r', 'rb', 'w', 'wb', 'a' or 'ab'. defaults to 'r'
-- @treturn[1] stream stream to the file
-- @treturn[2] nil
-- @treturn[2] string error message
function vfs.open(path, mode)
  checkArg(1, path, "string")
  mode = mode or "r"
  checkArg(2, mode, "string")
  if not mode_check[mode] then
    return nil, string.format("bad argument #2 (r[b], w[b] or a[b] expected, got %s)", mode)
  end

  local segments, err = vfs.path_to_segments(path, false, true)
  if not segments then
    return nil, err
  end

  local node, rest = findNode(segments, false, true)
  if not node then
    return nil, rest
  end

  if not node.fs or
    not rest or
    ((mode == "r" or mode == "rb") and not node.fs.exists(rest)) then
    return nil, "file not found"
  end

  local handle, reason = node.fs.open(rest, mode)
  if not handle then
    return nil, reason
  end

  return setmetatable({
    fs = node.fs,
    handle = handle,
    __type = "file",
  }, {
    __index = function(tbl, key)
      if not tbl.fs[key] then
        return
      end

      if not tbl.handle then
        return nil, "file is closed"
      end

      if key == "close" then
        return function(self, ...)
          local h = self.handle
          assert(h, "file already closed")
          self.handle = nil
          return self.fs[key](h, ...)
        end
      else
        return function(self, ...)
          local f = self.fs[key]

          if not f then
            return nil, "operation not supported"
          else
            return f(handle, ...)
          end
        end
      end
    end
  })
end

---
--
function vfs.makeDirectory(path)
  if vfs.exists(path) then
    return nil, "file or directory with that name already exists"
  end

  local segments, err = vfs.path_to_segments(path, false, true)
  if not segments then
    return nil, err
  end

  local node, rest = vfs.findNode(segments, false, true)
  if node.fs and rest then
    local success
    success, err = node.fs.makeDirectory(rest)
    if not success and not err and node.fs.isReadOnly() then
      err = "filesystem is readonly"
    end
    return success, err
  end

  if node.fs then
    return nil, "virtual directory with that name already exists"
  end

  return nil, "cannot create a directory in a virtual directory"
end

function vfs.lastModified(path)
  local segments, err = vfs.path_to_segments(path, false, true)
  if not segments then
    return nil, err
  end

  local node, rest, vnode, vrest = vfs.findNode(segments, false, true)
  if not node or not vnode.fs and not vrest then
    return 0 -- virt directory
  end

  if node.fs and rest then
    if node.fs.lastModified then
      return node.fs.lastModified(rest)
    else
      return 0
    end
  end

  return nil, "no such file or directory"
end

function vfs.mounts()
  local function path(node)
    local result = "/"
    while node and node.parent do
      for name, child in pairs(node.parent.children) do
        if child == node then
          result = "/" .. name .. result
        end
      end
      node = node.parent
    end
    return result
  end
  local queue = { mtab }
  return function()
    while #queue > 0 do
      local node = table.remove(queue)
      for _, child in pairs(node.children) do
        table.insert(queue, child)
      end

      if node.fs then
        -- FIXME: we don't just want to hand out node.fs like this
        return node.fs, path(node)
      end
    end
  end
end

function vfs.isLink(path)
  local _ = path
  return nil, "not implemented"
end

function vfs.link(target, linkpath)
  local _ = target
  local _ = linkpath
  return nil, "not implemented"
end

function vfs.umount_path_find(path)
  local segments, err = vfs.path_to_segments(path)
  if not segments then
    return nil, err
  end

  local _, _, vnode, vrest = findNode(segments, false, true)
  if not vrest and vnode.fs then
    return vnode
  end
end

local function removeEmptyNodes(node)
  while node and node.parent and not node.fs and not next(node.children) and not next(node.links) do
    node.parent.children[node.name] = nil
    node = node.parent
  end
end

function vfs.umount(fsOrPath)
  checkArg(1, fsOrPath, "string", "table")

  if type(fsOrPath == "string") then
    local segments, err = vfs.path_to_segments(fsOrPath)
    if not segments then
      return nil, err
    end

    local _, _, vnode, vrest = findNode(segments, false, true)
    if not vrest and vnode.fs then
      vnode.fs = nil
      removeEmptyNodes(vnode)
      return true
    end
  end

  local address = type(fsOrPath) == "table" and fsOrPath.address or fsOrPath

  -- FIXME: this can be optimised
  local result = false
  for proxy, path in vfs.mounts() do
    local addr = type(proxy) == "table" and proxy.address or proxy
    if string.sub(addr, 1, address:len()) == address then
      local segments, _ = vfs.path_to_segments(path)
      if segments then
        local _, _, vnode, _ = findNode(segments, false, true)
        vnode.fs = nil
        removeEmptyNodes(vnode)
        result = true
      end
    end
  end

  return result
end

function vfs.size(path)
  local segments, err = vfs.path_to_segments(path, false, true)
  if not segments then
    return nil, err
  end

  local node, rest, vnode, vrest = findNode(segments, false, true)
  if not node or not vnode.fs and (not vrest or vnode.links[vrest]) then
    return 0
  end

  if node.fs and rest then
    return node.fs.size(rest)
  end

  return 0
end

function vfs.unlink(path)
  local segments, err = vfs.path_to_segments(path, false, true)
  if not segments then
    return nil, err
  end

  local node, rest, vnode, vrest = findNode(segments, false, true)
  if not node then
    return nil, "no such file or directory"
  end
 -- assert(not vrest) -- TODO: implement

  if node.fs and rest then
    if node.fs.remove then
      return node.fs.remove(rest)
    end
  end

  return nil, "cannot remove"
end

vfs.remove = vfs.unlink

return vfs
