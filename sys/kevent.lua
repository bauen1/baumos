-- sys/kevent.lua - kernel events
-- vim: set tabstop=2 shiftwidth=2 expandtab :

--- event/signal callback implementation
-- @module kevent

local kevent = {}

kevent.listeners = {
  signal = {}
}

--- register a callback for a specific signal
-- @tparam string signal
-- @tparam function callback
function kevent.listen(signal, callback)
    if not kevent.listeners[signal] then
        kevent.listeners[signal] = {}
    end
    kevent.listeners[signal][#kevent.listeners[signal] + 1] = callback
end

function kevent.handle_signal(signal)
  for _, f in pairs(kevent.listeners.signal) do
    xpcall(f, function(e)
      klog("kevent listener error ('signal'): %s", tostring(e))
      klog("%s", debug.traceback())
    end, "signal", table.unpack(signal))
  end
end

return kevent
